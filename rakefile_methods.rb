## This file shall be kept in the root directory
require 'rubygems'
require 'term/ansicolor'
include Term::ANSIColor
require 'date'
require 'rake/clean'
require 'pathname'
require 'fileutils'

### Modify if needed
# Compiler
CC="gcc"
# General options
OPTIONS="-Wall -g"

# Compile a library to a dynamic lirbary (here for MacOS)
# This is system dependent
def to_dynamic_library(name)
    "-dynamiclib -dynamic -o lib#{name}.dylib"
end

### Do not change ################################################## 
# Root dir
ROOT=Pathname.new(File.dirname(__FILE__))
# For the compilation of the GSL
LIBGSL="-lgsl -lgslcblas -lm"
# Libraries (automatically filled)
LIBRARY=[]
# Libraries
NAMES_EXITBM = %w(exitbm exitbm_fz exitbm_fhor exitbm_square exitbm_rectangle exitbm_test exitbm_benchmark)
DIR_EXITBM_SRC = ROOT + "src"

# The names of the prerequisites also serves as the name of the library to link to.
def compile_library(t)
    extralib=t.prerequisites.map {|e| "-l%s" %[e]}.join(" ") || ""
    sh "%s %s %s.c -I. -L. %s %s %s" %[CC,OPTIONS,t.name,to_dynamic_library(t.name),extralib,LIBGSL]
    # add the name of the $library
    puts bold("#{green(t.name)} has been compiled as a dynamic library.") 
end

# name is a Symbol, a String or a Hash or two arguments with a Hash
def define_library(*name)
    t=Rake::Task.define_task(*name) do |t| compile_library(t) end
    name=name[0]
    case name
    when Symbol
	name=name.to_s
    when Hash
	name=name.keys[0].to_s
    end
    LIBRARY << name
    t.add_description("Compilation of #{name} as a dynamic library.")
end

# to link libraries
def link_library_here(name,dir)
    raise ArgumentError unless dir.is_a? Pathname
    libname="lib%s.dylib" %[name.to_s]
    unless File.exists?(libname) then
	FileUtils.ln_s(dir + libname,libname,:force=>true,:verbose=>true)
    end
end

# Header for the files
def header
return<<EOF
/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011,2012
**********************************************************************/
EOF
end
