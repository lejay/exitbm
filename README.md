This is the exitbm librabry, devoted to the simulation of the first exit 
time and position for the Brownian motion in simple domains.

http://exitbm.gforge.inria.fr/

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009, 2011,2012

Version 2.3

# USAGE

This is a C library relying on the GNU Scientific Library.

The code is documented with Doxygen. 

The goal, purpose and algorithm of the library is described in
the `doc/doc-exitbm.pd` file.

# INSTALLATION

This library may be compiled using the Rake utility and the provided `Rakefile`
(for mac os).
Otherwise, it may be compiled as any standard static or shared `C` library.

# FILES & DIRECTORIES

We only record here the source files.

`rakefile_methods.rb` is a set of ruby methods used by the Rakefile. 

```
applications/ For the applications
applications/media_with_double_layer/ Simulation in a double layer media
doc/ The documentation
doc/Rakefile Rakefile for the documentation
doc/doc-exitbm.tex Tex source of the documentation
doc/figures/ Figures for the documentation
doc/figures/library.dot Dot file (graphviz) of libraries dependencіes
doc/automatic-figures/ Automatically generated figures
doc/generation-of-figures/ Utilities to generate figures
doc/generation-of-figures/Rakefile Rakefile for the generation of figures
doc/generation-of-figures/documentation_figures.R R files for creating graphs
doc/generation-of-figures/documentation_figures.c C codes for creating graphs
src/ The sources
src/exitbm.c C Code of the library exitbm
src/exitbm.h C Header of the library exitbm
src/exitbm_fz.c C Code of the library exitbm_fz
src/exitbm_fz.h C Header of the library exitbm_fz
src/exitbm_fhor.c C Code of the library exitbm_fhor
src/exitbm_fhor.h C Header of the library exitbm_fhor
src/exitbm_square.c C Code of the library exitbm_square
src/exitbm_square.h C Header of the library exitbm_square
src/exitbm_rectangle.c C Code of the library exitbm_rectangle
src/exitbm_rectangle.h C Header of the library exitbm_rectangle
src/exitbm_test.c C Code of the library for the tests
src/exitbm_test.h C Header of the library for the tests
src/exitbm_benchmark.c C Code of the library for the benchmark
src/exitbm_benchmark.h C Header of the library for the benchmark
test/Rakefile Rakefile for benchmarks
test/test.c Several tests (not maintained)
test/test_functions.rb Creation of a C code with an interface to exitbm_test
```

