### This file is a part of the exitbm library (simulation of the first exit time and position
### for the Brownian motion in simple domains)
###
### http://exitbm.gforge.inria.fr/
### 
### This work is being provided under the free software CeCILL licence
### Antoine Lejay (TOSCA Project-team, INRIA), 2009,2010,2011,2012
##########################################################################################
require 'rubygems'
require './rakefile_methods'

FILES={}

LIBVERSION="2.3"

FILES[:C_FILES]=FileList.new(%w(
    src/exitbm.c
    src/exitbm_fz.c
    src/exitbm_fhor.c
    src/exitbm_square.c
    src/exitbm_rectangle.c
    src/exitbm_rectangle.c
    src/exitbm_benchmark.c
    src/exitbm_test.c
    doc/generation-of-figures/documentation_figures.c
    test/test.c
    applications/media_with_double_layer/ziggourat.c
    applications/media_with_double_layer/double_layer.c
))

FILES[:H_FILES]=FileList.new(%w(
    src/exitbm.h
    src/exitbm_fz.h
    src/exitbm_fhor.h
    src/exitbm_square.h
    src/exitbm_rectangle.h
    src/exitbm_rectangle.h
    src/exitbm_benchmark.h
    src/exitbm_test.h
    applications/media_with_double_layer/ziggourat.h
))

FILES[:TEX_FILES]=FileList.new(%w(
    doc/doc-exitbm.tex
))

FILES[:PDF_FILES]=FileList.new(%w(
    doc/doc-exitbm.pdf
    doc/figures/library.pdf
    doc/automatic-figures/*.pdf
))

FILES[:DOT_FILES]=FileList.new(%w(
    doc/figures/library.dot
))

FILES[:RAKEFILES]=FileList.new(%w(
    doc/Rakefile
    src/Rakefile
    Rakefile
    test/Rakefile
    applications/media_with_double_layer/Rakefile
    doc/generation-of-figures/Rakefile
    test_library/Rakefile))

FILES[:RUBYFILES]=FileList.new(%w(
    test/test_functions.rb
    rakefile_methods.rb
))

FILES[:DOXYFILES]=FileList.new(%w(
    src/Doxyfile
    test_library/Doxyfile
))

FILES[:R_FILES]=FileList.new(%w(
    doc/generation-of-figures/documentation_figures.R
))

FILES[:README] =FileList.new(%w(
    README
))

desc "Zip all the files"
task :zip do 
    files=[]
    FILES.each_pair do |k,v|
	files << v
    end
    sh "zip exitbm-%s.zip %s" %[LIBVERSION.sub(/\./,"-"),files.flatten.join(" ")]
end

