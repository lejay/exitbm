/**\file figure.c
 *
 * \brief Creation of the data files requires in order to create the figures of the documentation. 
 *
 * \warning This file has strong links with the R file \a figure.R which process the data. Any change in this file shall be reflected in \a figure.R.
 */
#include "exitbm_test.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/**\defgroup DocumentationFigures Generation of figures for the documentation*/
/*@{*/

/**\def OPEN
 * \brief Open a file.
 *
 * Each time a new file is open, write its name in \p figure.data
 * */
#define OPEN(stream,name) stream=fopen(name,"w");fprintf(stderr,"Creation of %s\n",name);assert(stream!=NULL);fprintf(data_file,"%s\n",name);

/**\brief Generate the set of files with the data for the figures.
 * 
 * These files will be processed with \a figure.R, using \a R.
 * */
int main()
{
    FILE *stream;
    FILE *data_file;
    data_file=fopen("figure.data","w");
    // The distribution function p_exit_time
    OPEN(stream,"p_exit_time_1");
    test_p_exit_time(stream,0.0,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_2");
    test_p_exit_time(stream,0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_3");
    test_p_exit_time(stream,0.9,5.0,0.01);
    fclose(stream);

    // The density d_exit_time
    OPEN(stream,"d_exit_time_1");
    test_d_exit_time(stream,0.0,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_2");
    test_d_exit_time(stream,0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_3");
    test_d_exit_time(stream,0.9,5.0,0.01);
    fclose(stream);

    // The distribution function p_exit_time_right
    OPEN(stream,"p_exit_time_right_1");
    test_p_exit_time(stream,-0.9,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_right_2");
    test_p_exit_time(stream,-0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_right_3");
    test_p_exit_time(stream,0.0,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_right_4");
    test_p_exit_time(stream,0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_right_5");
    test_p_exit_time(stream,0.9,5.0,0.01);
    fclose(stream);

    // The density d_exit_time_right
    OPEN(stream,"d_exit_time_right_1");
    test_d_exit_time(stream,-0.9,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_right_2");
    test_d_exit_time(stream,-0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_right_3");
    test_d_exit_time(stream,0.0,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_right_4");
    test_d_exit_time(stream,0.5,5.0,0.01);
    fclose(stream);
    OPEN(stream,"d_exit_time_right_5");
    test_d_exit_time(stream,0.9,5.0,0.01);
    fclose(stream);

    double x;
    // The distribution function of the position
    x=0.0;
    OPEN(stream,"p_pos_gne_1_1")
    test_p_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_1_2")
    test_p_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_1_3")
    test_p_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_1_4")
    test_p_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    x=0.5;
    OPEN(stream,"p_pos_gne_2_1")
    test_p_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_2_2")
    test_p_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_2_3")
    test_p_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_2_4")
    test_p_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    x=0.9;
    OPEN(stream,"p_pos_gne_3_1");
    test_p_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_3_2");
    test_p_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_3_3");
    test_p_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"p_pos_gne_3_4");
    test_p_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    // The density of the position
    x=0.0;
    OPEN(stream,"d_pos_gne_1_1")
    test_d_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_1_2")
    test_d_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_1_3")
    test_d_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_1_4")
    test_d_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    x=0.5;
    OPEN(stream,"d_pos_gne_2_1")
    test_d_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_2_2")
    test_d_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_2_3")
    test_d_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_2_4")
    test_d_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    x=0.9;
    OPEN(stream,"d_pos_gne_3_1");
    test_d_pos_gne(stream,0.1,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_3_2");
    test_d_pos_gne(stream,0.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_3_3");
    test_d_pos_gne(stream,1.5,x,0.01);
    fclose(stream);
    OPEN(stream,"d_pos_gne_3_4");
    test_d_pos_gne(stream,2.0,x,0.01);
    fclose(stream);

    OPEN(stream,"p_exit_time_hypercube_1")
    test_p_exit_time_hypercube(stream,1,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_hypercube_2")
    test_p_exit_time_hypercube(stream,2,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_hypercube_3")
    test_p_exit_time_hypercube(stream,3,5.0,0.01);
    fclose(stream);
    OPEN(stream,"p_exit_time_hypercube_4")
    test_p_exit_time_hypercube(stream,4,5.0,0.01);
    fclose(stream);

    OPEN(stream,"r_exit_time_hypercube_1")
    test_r_exit_time_hypercube(stream,100000,1);
    fclose(stream);
    OPEN(stream,"r_exit_time_hypercube_2")
    test_r_exit_time_hypercube(stream,100000,2);
    fclose(stream);
    OPEN(stream,"r_exit_time_hypercube_3")
    test_r_exit_time_hypercube(stream,100000,3);
    fclose(stream);
    OPEN(stream,"r_exit_time_hypercube_4")
    test_r_exit_time_hypercube(stream,100000,4);
    fclose(stream);
    fclose(data_file);

    fprintf(stderr,"Names of the created files are written in figure.data\n");

    return EXIT_SUCCESS;
}
/*@}*/
