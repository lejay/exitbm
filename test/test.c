#include "exitbm.h"
#include "exitbm_fz.h"
#include "exitbm_square.h"
#include "exitbm_rectangle.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_log.h"
#include "gsl/gsl_sf_log.h"
#include "gsl/gsl_randist.h"

/** @file test.c 
 *
 * @brief Various test functions (not maintained) */

#define NB 100000
#define MILLION 1000000

/** \brief Print in a file the distribution function of the first exit time as a function of the time.
 *
 * \param[in] x Starting point
 */
void plot_p_exit_time(double x) 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/p_exit_time_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,p_exit_time(t,x));
    fclose(stream);
}

/** \brief Plot \a q_exit_time
 *
 * \param[in] x Starting point
 *
 * The third column and the first should be equal.
 */
void plot_q_exit_time(double x) 
{
    double value,res;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/q_exit_time_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(value=0.001;value<1.0;value+=0.1)
	fprintf(stream,"%f\t%f\t%f\n",value,res=q_exit_time(value,x,1.0),p_exit_time(res,x));
    fclose(stream);
}

/** \brief Print in a file the distribution of the first exit time as a function of the time when the starting point is zero.
 */
void plot_p_exit_time_fz() 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/p_exit_time_fz");
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,p_exit_time_fz(t));
    fclose(stream);
}

/** \brief Print in a file the distribution of the first exit time from an hypercube as a function of the time when the starting point is zero.
 */
void plot_p_exit_time_hypercube(unsigned int dim) 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/p_exit_time_hypercube_%i",dim);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,p_exit_time_hypercube(t,dim));
    fclose(stream);
}

/** \brief Plot \a q_exit_time_fz
 *
 * The third column and the first should be equal.
 */
void plot_q_exit_time_fz() 
{
    double value,res;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/q_exit_time_fz");
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(value=0.001;value<1.0;value+=0.1)
	fprintf(stream,"%f\t%f\t%f\n",value,res=q_exit_time_fz(value),p_exit_time_fz(res));
    fclose(stream);
}

/** \brief Print in a file the density of the first exit time as a function of the time.
 *
 * \param[in] x Starting point
 */

void plot_d_exit_time(double x) 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/d_exit_time_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,d_exit_time(t,x));
    fclose(stream);
}

/** \brief Perform a numerical integration of the density of the first exit time and compare it with the distribution function.
 *
 * \param[in] x Starting point
 */
void integration_d_exit_time(double x) 
{
    double t,sum=0.0,step=0.001,pet;
    for(t=0.0001;t<10.0;t+=step)
    {
	sum+=d_exit_time(t,x)*step;
	if (gsl_fcmp(sum,pet=p_exit_time(t,x),1e-3))
	    printf("%f\t%e\t%e\t%e\n",t,sum-pet,sum,pet);
    }
}

/** \brief Print some realizations of the first exit time.
 *
 * \param[in] x Starting point
 */
void realization_r_exit_time(double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time(rng,x));
}

/** \brief Print some realizations of the first exit time given it is smaller than an arbitrary value.
 *
 * \param[in] t Maximal time
 * \param[in] x Starting point
 */
void realization_r_exit_time_gets(double t,double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time_gets(rng,t,x));
}

/** \brief Print some realizations of the first exit time when 
 * the starting point is zero.
 *
 */
void realization_r_exit_time_fz()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time_fz(rng));
}

/** \brief Print some realizations of the first exit time given it is smaller than an arbitrary value, when the starting point is zero.
 *
 * \param[in] t Maximal time
 */
void realization_r_exit_time_gets_fz(double t)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time_gets_fz(rng,t));
}

/** \brief Print in a file the distribution function of the first exit time as a function of the time, when the Brownian motion leaves by the right endpoint.
 *
 * \param[in] x Starting point
 */
void plot_p_exit_time_right(double x) 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/p_exit_time_right_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,p_exit_time_right(t,x));
    fclose(stream);
}

/** \brief Plot \a q_exit_time_right
 *
 * The third column and the first should be equal.
 */
void plot_q_exit_time_right(double x) 
{
    double value,res;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/q_exit_time_right_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(value=0.001;value<1.0;value+=0.01)
	fprintf(stream,"%f\t%f\t%f\n",value,res=q_exit_time_right(value,x,1.0),p_exit_time_right(res,x));
    fclose(stream);
}

/** \brief Print in a file the density of the first exit time as a function of the time, when the Brownian motion leaves by the right endpoint.
 *
 * \param[in] x Starting point
 */

void plot_d_exit_time_right(double x) 
{
    double t;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/d_exit_time_right_%.2f",x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(t=0.001;t<10.0;t+=0.1)
	fprintf(stream,"%f\t%f\n",t,d_exit_time_right(t,x));
    fclose(stream);
}

/** \brief Perform a numerical integration of the density of the first exit time and compare it with the distribution function, when the Brownian motion leaves by the right endpoint.
 *
 * \param[in] x Starting point
 */
void integration_d_exit_time_right(double x) 
{
    double t,sum=0.0,step=0.001,pet;
    for(t=0.0001;t<10.0;t+=step)
    {
	sum+=d_exit_time_right(t,x)*step;
	if (gsl_fcmp(sum,pet=p_exit_time_right(t,x),1e-3))
	    printf("%f\t%e\t%e\t%e\n",t,sum-pet,sum,pet);
    }
}

/** \brief Print some realizations of the first exit time, when the Brownian motion leaves by the right endpoint.
 *
 *
 * \param[in] x Starting point
 */
void realization_r_exit_time_right(double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time_right(rng,x));
}

/** \brief Print some realizations of the first exit time given it is smaller than an arbitrary value, when the Brownian motion leaves by the right endpoint.
 *
 * \param[in] t Maximal time
 * \param[in] x Starting point
 */
void realization_r_exit_time_right_gets(double t,double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    printf("%f\n",r_exit_time_right_gets(rng,t,x));
}

/**\brief Create the files for some figures for the documentation
 * (see also the file \a graph.r)
 */
void figures_exit_time_right() 
{
	plot_d_exit_time_right(-0.9);
	plot_d_exit_time_right(-0.5);
	plot_d_exit_time_right(0.0);
	plot_d_exit_time_right(0.5);
	plot_d_exit_time_right(0.9);
	plot_p_exit_time_right(-0.9);
	plot_p_exit_time_right(-0.5);
	plot_p_exit_time_right(0.0);
	plot_p_exit_time_right(0.5);
	plot_p_exit_time_right(0.9);
}

/**\brief Print into a file the density function \a p_pos_gne.
 */
void plot_d_pos_gne(double t, double x)
{
    double y;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/d_pos_gne_%.2f_%.2f",t,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(y=-0.99;y<0.99;y+=0.01)
	fprintf(stream,"%f\t%f\n",y,d_pos_gne(y,t,x));
    fclose(stream);
}

/**\brief Compare the numerical integral of the density with the distribution function.
 */
void integrate_d_pos_gne(double t, double x)
{
    double y;
    double sum=0.0,step=0.001;
    double pet,diff;
    for(y=-0.99;y<0.99;y+=step)
    {
	sum+=d_pos_gne(y,t,x)*step;
	pet=p_pos_gne(y,t,x);
	diff=sum-pet;
	if (fabs(diff)>1e-3)
	    printf("%f\t%e\t%e\t%e\n",y,diff,sum,pet);
    }
}

/**\brief Print into a file the distribution function \a p_pos_gne.
 */
void plot_p_pos_gne(double t, double x)
{
    double y;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/p_pos_gne_%.2f_%.2f",t,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(y=-0.99;y<0.99;y+=0.01)
	fprintf(stream,"%f\t%f\n",y,p_pos_gne(y,t,x));
    fclose(stream);
}

/**\brief Creates some files for the figures for the documentation.
 * (see also the file \a graph.r)
 */

void figures_pos_gne()
{
	double x;
	x=0.0;
	plot_p_pos_gne(0.1,x);
	plot_p_pos_gne(0.5,x);
	plot_p_pos_gne(1.5,x);
	plot_p_pos_gne(2.0,x);
	plot_d_pos_gne(0.1,x);
	plot_d_pos_gne(0.5,x);
	plot_d_pos_gne(1.5,x);
	plot_d_pos_gne(2.0,x);
	x=0.5;
	plot_p_pos_gne(0.1,x);
	plot_p_pos_gne(0.5,x);
	plot_p_pos_gne(1.5,x);
	plot_p_pos_gne(2.0,x);
	plot_d_pos_gne(0.1,x);
	plot_d_pos_gne(0.5,x);
	plot_d_pos_gne(1.5,x);
	plot_d_pos_gne(2.0,x);
	x=0.9;
	plot_p_pos_gne(0.1,x);
	plot_p_pos_gne(0.5,x);
	plot_p_pos_gne(1.5,x);
	plot_p_pos_gne(2.0,x);
	plot_d_pos_gne(0.1,x);
	plot_d_pos_gne(0.5,x);
	plot_d_pos_gne(1.5,x);
	plot_d_pos_gne(2.0,x);
}

/** \brief Plot \a q_pos_gne
 *
 * \param[in] x Starting point
 *
 * The third column and the first should be equal.
 */
void plot_q_pos_gne(double t,double x) 
{
    double value,res;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/q_pos_gne_%.2f_%.2f",t,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
    for(value=0.001;value<1.0;value+=0.001)
	fprintf(stream,"%f\t%f\t%f\n",value,res=q_pos_gne(value,t,x),p_pos_gne(res,t,x));
    fclose(stream);
}

/** \brief Print some realizations of the position of the Brownian motion given it has not exit from the interval.
 *
 * \param[in] t Time of the marginal
 * \param[in] x Starting point
 */
void realization_r_pos_gne(double t,double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_pos_gne_%.2f_%.2f",t,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    fprintf(stream,"%f\n",r_pos_gne(rng,t,x));
    fclose(stream);
}

/** \brief Print some realizations of the position of the Brownian motion given it has not exit from the interval and the exit time is greater than a given time.
 *
 * \param[in] t Time of the marginal
 * \param[in] theta Minimal exit time
 * \param[in] x Starting point
 */
void realization_r_pos_getg(double t,double theta, double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_pos_getg_%.2f_%.2f_%.2f",t,theta,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    fprintf(stream,"%f\n",r_pos_getg(rng,t,theta,x));
    fclose(stream);
}

/** \brief Print some realizations of the position of the Brownian motion given it has not exit from the interval and the exit time is equal to a given time.
 *
 * \param[in] t Time of the marginal
 * \param[in] theta Exit time
 * \param[in] x Starting point
 */
void realization_r_pos_right_gete(double t,double theta, double x)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_pos_right_gete_%.2f_%.2f_%.2f",t,theta,x);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    fprintf(stream,"%f\n",r_pos_right_gete(rng,t,theta,x));
    fclose(stream);
}

void benchmark_r_exit_time()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double x;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_exit_time(rng,x);
	    }
}

void benchmark_r_exit_time_fz()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	    {
	    r_exit_time_fz(rng);
	    }
}

void benchmark_r_exit_time_gets()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t,x;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_gets(rng,t,x);
	    }
}

void benchmark_r_exit_time_gets_fz()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_gets_fz(rng,t);
	    }
}

void benchmark_r_exit_time_right()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double x;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_exit_time_right(rng,x);
	    }
}

void benchmark_r_exit_time_right_gets()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t,x;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_right_gets(rng,t,x);
	    }
}

void benchmark_r_pos_gne()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t,x;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_gne(rng,t,x);
	    }
}

void benchmark_r_pos_gne_fz()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    r_pos_gne_fz(rng,t);
	    }
}

void benchmark_r_pos_getg()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t,x,theta;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    theta=gsl_ran_exponential(rng,1.0);
	    t=gsl_rng_uniform(rng)*theta;
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_getg(rng,t,theta,x);
	    }
}

void realization_r_exit_time_hypercube(unsigned int dim)
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_time_hypercube_%i",dim);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
	fprintf(stream,"%f\n",r_exit_time_hypercube(rng,dim));
    fclose(stream);
}

void figures_square()
{
    plot_p_exit_time_hypercube(1);
    plot_p_exit_time_hypercube(2);
    plot_p_exit_time_hypercube(3);
    plot_p_exit_time_hypercube(4);
    realization_r_exit_time_hypercube(1);
    realization_r_exit_time_hypercube(2);
    realization_r_exit_time_hypercube(3);
    realization_r_exit_time_hypercube(4);
}

void benchmark_r_pos_gete()
{
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    double t,x,theta;
    unsigned int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
	    {
	    theta=gsl_ran_exponential(rng,1.0);
	    t=gsl_rng_uniform(rng)*theta;
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_right_gete(rng,t,theta,x);
	    }
}

void realization_r_exit_time_position_space_time_square(double final_time)
{
    double x,y,time;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_space_time_square_%.2f",final_time);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
    {
	r_exit_time_position_space_time_square(rng,final_time,&time,&x,&y);
	fprintf(stream,"%f\t%f\t%f\n",time,x,y);
    }
    fclose(stream);


}

void benchmark_r_exit_time_position_space_time_square()
{
    double x,y,time,final_time;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
    {
	final_time=gsl_ran_exponential(rng,1.0);
	r_exit_time_position_space_time_square(rng,final_time,&time,&x,&y);
    }


}

void benchmark_r_exit_time_position_square()
{
    double x,y,time;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
    {
	r_exit_time_position_square(rng,&time,&x,&y);
    }


}

void realization_r_exit_time_position_square()
{
    double x,y,time;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_square");
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
    {
	r_exit_time_position_square(rng,&time,&x,&y);
	fprintf(stream,"%f\t%f\t%f\n",time,x,y);
    }
    fclose(stream);

}

void realization_r_exit_time_position_rectangle(double half_width,
	double half_height, double init_x, double init_y)
{
    double x,y,time;
    double sq_half_width=half_width*half_width;
    double sq_half_height=half_height*half_height;
    int side;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_rectangle_%.2f_%.2f_%.2f_%.2f",half_width,half_height,init_x,init_y);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
    {
	r_exit_time_position_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
    fclose(stream);
}

void realization_r_exit_time_position_rectangle_rws(double half_width,
	double half_height, double init_x, double init_y)
{
    double x,y,time;
    int side;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_rectangle_rws_%.2f_%.2f_%.2f_%.2f",half_width,half_height,init_x,init_y);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
    {
	r_exit_time_position_rectangle_rws(rng,half_width,half_height,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
    fclose(stream);
}

void realization_r_exit_time_position_space_time_rectangle(
	double half_width,
	double half_height,
	double final_time,
	double init_x, double init_y)
{
    double x,y,time;
    double sq_half_width=half_width*half_width;
    double sq_half_height=half_height*half_height;
    int side;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
    FILE *stream;
    char nom[256];
    sprintf(nom,"Results/r_exit_space_time_rectangle_%.2f_%.2f_%.2f_%.2f",half_width,half_height,init_x,init_y);
    stream=fopen(nom,"w");
    assert(stream!=NULL);
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<NB;i++)
    {
	r_exit_time_position_space_time_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,final_time,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
    fclose(stream);
}

void benchmark_r_exit_time_position_rectangle()
{
    double x,y,time;
    double sq_half_width;
    double sq_half_height;
    int side;
    double half_width, half_height, init_x, init_y;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	sq_half_width=half_width*half_width;
	sq_half_height=half_height*half_height;
	r_exit_time_position_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,init_x,init_y,&time,&x,&y,&side);
    }
}

void benchmark_r_exit_time_position_rectangle_rws()
{
    double x,y,time;
    int side;
    double half_width, half_height, init_x, init_y;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	r_exit_time_position_rectangle_rws(rng,half_width,half_height,init_x,init_y,&time,&x,&y,&side);
    }
}

void benchmark_r_exit_time_position_space_time_rectangle()
{
    double x,y,time;
    double sq_half_width;
    double sq_half_height;
    int side;
    double half_width, half_height, init_x, init_y;
    double final_time;

    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
    int i;
	
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);

    for(i=0;i<MILLION;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	final_time=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	sq_half_width=half_width*half_width;
	sq_half_height=half_height*half_height;
	r_exit_time_position_space_time_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,final_time,init_x,init_y,&time,&x,&y,&side);
    }
}

int main()
{
    benchmark_r_exit_time_position_rectangle_rws();
	//realization_r_exit_time_position_rectangle(2.0,1.0,0.5,0.8);
	//realization_r_exit_time_position_rectangle_rws(2.0,1.0,0.5,0.8);
	//benchmark_r_exit_time_position_rectangle();
	//benchmark_r_exit_time_position_space_time_rectangle();
	//realization_r_exit_time_position_space_time_rectangle(1.0,2.0,1.0,0.5,0.3);
	//benchmark_r_exit_time_position_square();
	//benchmark_r_exit_time_position_space_time_square();
	//realization_r_exit_time_position_square();
	//realization_r_exit_time_position_space_time_square(1.0);
	//figures_square();
	//benchmark_r_exit_time_gets_fz();
	//benchmark_r_pos_gne_fz();
	//benchmark_r_exit_time();
	//benchmark_r_exit_time_right();
	//benchmark_r_pos_gne();
	//benchmark_r_pos_gete();
	//figures_pos_gne();
	//integrate_d_pos_gne(1.5,0.97);
	//realization_r_pos_getg(1.0,2.0,0.0);
	//figures_exit_time_right();
	//plot_q_exit_time(0.50);
	//plot_p_exit_time_fz(0.9);
	//integration_d_exit_time_right(0.9);
	//realization_r_exit_time_right(0.5);
	return EXIT_SUCCESS;
}
