/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
/** @file exitbm_fhor.c
 
  @brief Simulation of the first exit time and position from an interval provided it happens before a given time, or the position at this time.
 
  @author Antoine Lejay
  @version 2.3
  @date April 20, 2012
**/

#include <exitbm_fhor.h>

/** \addtogroup intervals */
//@{

/**\brief Construction of a new interval 
 *
 * @param left Left-end points of the interval.
 * @param right Right-end point of the interval.
 * @param init Initial position of the Brownian motion.
*/
interval *interval_new(double left, double right, double init)
{
    // These 3 lines cause a parseerror to splint !
#ifdef ASSERT_MODE
    assert(left<right);
    assert(left<init);
    assert(init<right);
#endif

    interval *inter;
    inter=malloc(sizeof(interval));
    if(inter==NULL)
	{ fprintf(stderr,"Out of memory!");exit(EXIT_FAILURE);}
    inter->left=left;
    inter->right=right;
    inter->init=init;
    inter->scale=(right-left)/2.0;
    inter->sq_scale=inter->scale*inter->scale;
    inter->sc_init=2*(init-left)/(right-left)-1.0;
    return(inter);
}

/**\brief Construction of a new centered interval 
 *
 * The corresponds to an interval \f$[x-L,x+L]\f$, where \f$x\f$ is the starting point of the (Skew) Brownian motion.
 * @param center Position \f$x\f$ of the center of the interval.
 * @param half_width Half-width \f$L\f$ of the interval.
*/
cent_interval * cent_interval_new(double center, double half_width)
{
    cent_interval *inter;
    inter=malloc(sizeof(cent_interval));
    if(inter==NULL)
	{ fprintf(stderr,"Out of memory!");exit(EXIT_FAILURE);}
    inter->center=center;
    inter->half_width=half_width;
#ifdef ASSERT_MODE
    assert(half_width>0.0);
#endif
    inter->scale=half_width;
    inter->sq_scale=half_width*half_width;
    return(inter);
}

/** \fn void interval_inspect(interval *inter,FILE *file)
 * \brief Inspection of an interval.
 *
 * @param *inter An interval.
 * @param *file A stream on which the output is written.
 */

void interval_inspect(interval *inter,FILE *file)
{
    fprintf(file,"left = %f\tright = %f\tinit=%f\n",inter->left,inter->right,inter->init);
    fprintf(file,"scale = %f\tsq_scale = %f\tsc_init=%f\n",inter->scale,inter->sq_scale,inter->sc_init);
}


/** 
 * \brief Inspection of a centered interval.
 *
 * @param *inter A centered interval.
 * @param *file A stream on which the output is written.
 */

void cent_interval_inspect(cent_interval *inter, FILE *file)
{
    fprintf(file,"center = %f\thalf-width = %f\tscale = %f\t sq_scale = %f\n",inter->center,inter->half_width,inter->scale,inter->sq_scale);
}

//@}

/** @addtogroup random_variate_bm */
//@{
/**
 * \brief Exit time and position from the interval \f$[-1,1]\f$ for the Brownian motion before a given horizon
 *
 * Returns the exit time and position from the interval \f$[-1,1]\f$ for the Brownian motion starting from \p init_pos if it happens before \p horizon, or the position at time \p horizon of the Brownian motion given it has not left the interval \f$[-1,1]\f$ at time \p horizon.
 *
 * @param *rng Random number generator
 * @param horizon The horizon
 * @param init_pos Initial position
 * @param[out] time Exit time or horizon
 * @param[out] position Position at exit time or at time \p horizon
 * @param[out] exit Takes value 1 if the particle exits from the right before \p horizon, -1 is the particle exits from the left before \p horizon and 0 otherwise
 */

void r_exit_time_position_before_horizon(gsl_rng *rng,double horizon, double init_pos, double *time, double *position, int *exit)
{
    double proba_exit=p_exit_time(horizon,init_pos);
    double proba_exit_right;
    if (gsl_rng_uniform(rng)<=proba_exit) // The particle exits before horizon
    {
	// This probability to exit from the right is obtained by the Baye's rule.
	proba_exit_right=p_exit_time_right(horizon,init_pos)*(1.0+init_pos)/(2*proba_exit);
	*exit=(gsl_rng_uniform(rng)<=proba_exit_right ? 1 : -1);
	if(*exit==1) // if the exit occurs by the right
	    {*time=r_exit_time_right_gets(rng,horizon,init_pos);}
	else // if the exit occurs by the left: use a symmetry
	    {*time=r_exit_time_right_gets(rng,horizon,-init_pos);}
	*position=(float) *exit;
    }
    else
    {
	*exit=0;
	*time=horizon;
	*position=r_pos_gne(rng,horizon,init_pos);
    }
}
//@}



/** @addtogroup random_variate_bm */
//@{
/**
 * \brief Exit time and position from the interval \f$[-1,1]\f$ for the Brownian motion starting from zero before a given horizon.
 *
 * Returns the exit time and position from the interval \f$[-1,1]\f$ for the Brownian motion starting from \f$0.0\f$ if it happens before \p horizon, or the position at time \p horizon of the Brownian motion given it has not left the interval \f$[-1,1]\f$ at time \p horizon.
 *
 * @param *rng Random number generator
 * @param horizon The horizon
 * @param[out] time Exit time or horizon
 * @param[out] position Position at exit time or at time \p horizon
 * @param[out] exit Takes value 1 if the particle exits from the right before \p horizon, -1 is the particle exits from the left before \p horizon and 0 otherwise
 */

void r_exit_time_position_before_horizon_fz(gsl_rng *rng, double horizon, double *time, double *position, int *exit)
{
    double proba_exit=p_exit_time_fz(horizon);
    if (gsl_rng_uniform(rng)<=proba_exit)
    {
	*time=r_exit_time_gets_fz(rng,horizon);
	*exit=(gsl_rng_uniform(rng)<=0.5 ? 1 : -1);
	*position=(float) *exit;
    }
    else
    {
	*exit=0;
	*time=horizon;
	*position=r_pos_gne_fz(rng,horizon);
    }
}
//@}



/** @addtogroup random_variate_sbm */
//@{
/**
 * \brief Exit time and position from the interval \f$[-1,1]\f$ for the Skew Brownian motion starting from zero before a given horizon.
 *
 * Returns the exit time and position from the interval \f$[-1,1]\f$ for the Skew Brownian motion of parameter \p skewness starting from \f$0.0\f$ if it happens before \p horizon, or the position at time \p horizon of the Brownian motion given it has not left the interval \f$[-1,1]\f$ at time \p horizon.
 *
 * @param *rng Random number generator.
 * @param horizon The horizon.
 * @param skewness The Skewness paramater, shall belongs to \f$[0,1]\f$, (\f$0.5\f$ corresponds to Brownian motion).
 * @param[out] time Exit time or horizon.
 * @param[out] position Position at exit time or at time \p horizon.
 * @param[out] exit Takes value 1 if the particle exits from the right before \p horizon, -1 is the particle exits from the left before \p horizon and 0 otherwise.
 */

void r_exit_time_position_before_horizon_sbm(gsl_rng *rng, double horizon, double skewness,  double *time, double *position, int *exit)
{
    double proba_exit=p_exit_time_fz(horizon);
    if (gsl_rng_uniform(rng)<=proba_exit)
    {
	*time=r_exit_time_gets_fz(rng,horizon);
	*exit=(gsl_rng_uniform(rng)<=skewness ? 1 : -1);
	*position=(float) *exit;
    }
    else
    {
	*exit=0;
	*time=horizon;
	*position=fabs(r_pos_gne_fz(rng,horizon))*(gsl_rng_uniform(rng)<=skewness ? 1 : -1);
    }
}
//@}


/** \addtogroup random_variate_bm */
//@{

/**
 * \brief Exit time and position from an interval for the Brownian motion before a given horizon
 *
 * Returns the exit time and position from the interval given by \p inter for the Brownian motion starting from \p init_pos if it happens before \p horizon, or the position at time \p horizon of the Brownian motion given it has not left the interval at time \p horizon.
 *
 * @param *rng Random number generator
 * @param inter Interval
 * @param horizon The horizon
 * @param[out] time Exit time or horizon
 * @param[out] position Position at exit time or at time \p horizon
 * @param[out] exit Takes value 1 if the particle exits from the right before \p horizon, -1 is the particle exits from the left before \p horizon and 0 otherwise.
 */

void r_exit_time_position_before_horizon_from_interval(gsl_rng *rng, interval *inter, double horizon, double *time, double *position, int *exit)
{
    r_exit_time_position_before_horizon(rng,horizon/inter->sq_scale,inter->sc_init,time,position,exit);
    (*time) *= inter->sq_scale;
    (*position) =inter->scale*((*position)+1.0)+inter->left;
}
//@}



/** @addtogroup random_variate_sbm */
//@{
/**
 * \brief Exit time and position from an interval for the Skew Brownian motion before a given horizon.
 *
 * Returns the exit time and position from the interval given by \p inter for the Brownian motion starting from \p init_pos if it happens before \p horizon, or the position at time \p horizon of the Brownian motion given it has not left the interval at time \p horizon.
 *
 * @param *rng Random number generator
 * @param inter A centered interval
 * @param horizon The horizon
 * @param skewness The Skewness paramater, shall belongs to \f$[0,1]\f$, (\f$0.5\f$ corresponds to Brownian motion).
 * @param[out] time Exit time or horizon
 * @param[out] position Position at exit time or at time \p horizon
 * @param[out] exit Takes value 1 if the particle exits from the right before \p horizon, -1 is the particle exits from the left before \p horizon and 0 otherwise.
 */


void r_exit_time_position_before_horizon_from_interval_sbm(gsl_rng *rng, cent_interval *inter, double horizon, double skewness, double *time, double *position, int *exit)
{   
    double sc_horizon=horizon/inter->sq_scale;
    r_exit_time_position_before_horizon_sbm(rng, sc_horizon, skewness, time, position, exit);
    (*time) *= inter->sq_scale;
    *position= inter->center + inter->half_width * (*position);
}
//@}
