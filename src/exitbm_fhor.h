/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
#ifndef EXITBM_FHOR_H
#define EXITBM_FHOR_H
/** @file exitbm_fhor.h
 
  @brief Simulation of the first exit time and position from an interval provided it happens before a given time, or the position at this time.
 
  @author Antoine Lejay
  @version 2.3
  @date April 20, 2012
 **/
#include <exitbm/exitbm.h>
#include <exitbm/exitbm_fz.h>
#include <assert.h>
#include <stdlib.h>

/** \defgroup random_variate_bm Random variates related to the exit time and position for the Brownian motion.
 * \brief Generation of random variates related to the exit time and position for the Brownian motion, or its position at a given time.
 */

/** \defgroup random_variate_sbm Generation of random variates related to the exit time and position of the Skew Brownian motion.
 * \brief Generation of random variates related to the exit time and position for the skew Brownian motion, or its position at a given time.
 */


/** \defgroup intervals Structures and functions for encoding intervals.
 * \brief Structure and function to manipulate intervals.
 */

/** @addtogroup intervals */
//@{

/** \struct _interval
 * \brief Structure for intervals.
 *
 * This structure contains informations about the way to reduce the interval to \f$[-1,1]\f$. 
 */
struct _interval {
    /** Left-end point of the interval. */
    double left;
    /** Right-end point of the interval. */
    double right;
    /** Position of the starting point of the Brownian motion. */
    double init;
    /** Scaled position of the starting point of the Brownian motion, ie when the interval is scaled and shifted to be \f$[-1,1]\f$. */
    double sc_init;
    /** Scale to apply to transform the interval into \f$[-1,1]\f$. Used to normalized the space variable. */
    double scale;
    /** Square of the scale to apply to transform the interval into \f$[-1,1]\f$. Used to normalize the time variable. */
    double sq_scale;
};


/** \struct _cent_interval
 * \brief Structure for symmetric interval.
 *
 * This kind of interval is used the the Skew Brownian motion.
 */
struct _cent_interval {
    /** Position of the center of the interval */
    double center;
    /** Half-width of the interval */
    double half_width;
    /** Scale factor to apply */
    double scale;
    /** Square of the scale factor */
    double sq_scale;
};

/** \typedef interval 
 *\brief Type for intervals */
typedef struct _interval interval;
/** \typedef cent_interval 
 * \brief Type for centered intervals */
typedef struct _cent_interval cent_interval;
//@}


interval * interval_new(double left, double right, double init);


cent_interval * cent_interval_new(double center, double half_width);


void interval_inspect(interval *inter,FILE *file);

void cent_interval_inspect(cent_interval *inter, FILE *file);

void r_exit_time_position_before_horizon(gsl_rng *rng,double horizon, double init_pos, double *time, double *position, int *exit);

void r_exit_time_position_before_horizon_fz(gsl_rng *rng, double horizon, double *time, double *position, int *exit);


void r_exit_time_position_before_horizon_sbm(gsl_rng *rng, double horizon, double skewness,  double *time, double *position, int *exit);


void r_exit_time_position_before_horizon_from_interval(gsl_rng *rng, interval *inter, double horizon, double *time, double *position, int *exit);


void r_exit_time_position_before_horizon_from_interval_sbm(gsl_rng *rng, cent_interval *inter, double horizon, double skewness, double *time, double *position, int *exit);

#endif
