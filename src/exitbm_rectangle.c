/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
/** @file exitbm_rectangle.c 

@brief Functions for simulating the first exit time and position 
from a rectangle.

Simulate the first exit time and position
from a rectangle, or from a time-space
parallelepiped, which means the first 
exit time and position if it happens before
a fixed time, or the position at this time
if the particle has not left the rectangle
at this time.

Uses the library @link exitbm.c exitbm@endlink,
@link exitbm_fz.c exitbm_fz@endlink and
@link exitbm_square.c exitbm_square@endlink.

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#include <exitbm_rectangle.h>
#include <assert.h>


/** @defgroup ExitTimePositionRectangle Exit time and position from a rectangle  
 
@brief Simulate the first exit time and position from a rectangle
for a Brownian particle. 
*/


#include <exitbm_rectangle.h>
#include <exitbm_square.h>
#include <exitbm_fz.h>


/** @addtogroup ExitTimePositionRectangle */
/*@{*/

/**********************************************************************
EXIT TIME AND POSITION FROM A RECTANGLE
**********************************************************************/

/** \brief Simulate the exit time and position from a rectangle
 * using the random walk on squares.
 *
 * \param[in] rng Random number generator.
 * \param[in] half_width Half of the width of the rectangle.
 * \param[in] half_height Half the height of the rectangle.
 * \param[in] xpos x-coordinate of the initial position.
 * \param[in] ypos y-coordinate of the initial position.
 * \param[out] exit_time exit time.
 * \param[out] x x-coordinate of the exit position.
 * \param[out] y y-coordinate of the exit position.
 * \param[out] side side of the exit.
	-0 bottom
	-1 right
	-2 up
	-3 left
 *
 * This method aims to be faster than \a r_exit_time_position_rectangle, as \a r_exit_time_position_square uses tabulated values.
 */

int r_exit_time_position_rectangle_rws(gsl_rng *rng,
	double half_width, double half_height,
	double xpos, double ypos,
	double *exit_time, double *x, double *y,
	int *side)
{
    double new_time;
    double new_x,new_y;
    double dist_x,dist_y,dist;
    typedef enum SIDE {BOT=0,RIGHT=1,TOP=2,LEFT=3} index_side;
    // Array of booleans regarding the sides by which the particle may exit.
    int side_possible[4];
    // No exit
    *side=5;
    *x=xpos;
    *y=ypos;
    *exit_time=0.0;
    while (*side==5) {
	side_possible[BOT]=0;	
	side_possible[RIGHT]=0;	
	side_possible[TOP]=0;	
	side_possible[LEFT]=0;	
	if(*x>0.0)
	{
	   dist_x=half_width-*x;  
	   side_possible[RIGHT]=1;
	}
	else if(*x<0.0)
	{
	   dist_x=*x+half_width;  
	   side_possible[LEFT]=1;
	}
	else if(*x==0.0)
	{
	    dist_x=half_width;
	    side_possible[LEFT]=1;
	    side_possible[RIGHT]=1;
	}
	if(*y>0.0)
	{
	   dist_y=half_height-*y;  
	   side_possible[TOP]=1;
	}
	else if(*y<0.0)
	{
	   dist_y=*y+half_height;  
	   side_possible[BOT]=1;
	}
	else if(*y==0.0)
	{
	    dist_y=half_height;
	    side_possible[TOP]=1;
	    side_possible[BOT]=1;
	}
	if (dist_x<dist_y)
	{
	    side_possible[TOP]=0;
	    side_possible[BOT]=0; 
	    dist=dist_x;
	}
	else if (dist_x>dist_y)
	{
	    side_possible[LEFT]=0;
	    side_possible[RIGHT]=0; 
	    dist=dist_y;
	}
	else 
	    dist=dist_x;
#ifdef ASSERT_MODE
	assert(r_exit_time_position_square(rng,&new_time,&new_x,&new_y));
#else
	r_exit_time_position_square(rng,&new_time,&new_x,&new_y);
#endif
	if (side_possible[RIGHT] && (new_x==1.0))
	    *side=1;
	if (side_possible[LEFT] && (new_x==-1.0))
	    *side=3;
	if (side_possible[TOP] && (new_y==1.0))
	    *side=2;
	if (side_possible[BOT] && (new_y==-1.0))
	    *side=0;
	*x+=new_x*dist;
	*y+=new_y*dist;
	*exit_time+=gsl_pow_2(dist)*new_time;
    };
    return 1;
}

/** \brief Simulate the exit time and position from a rectangle for the Brownian motion.

@param *rng Random number generator.
@param half_width  Half of the width of the rectangle.
@param half_height Half of the height of the rectangle.
@param SQ_half_width square of half_width (not checked).
@param SQ_half_height square of half_height (not checked).
@param xpos x-coordinate of the initial position.
@param ypos y-coordinate of the initial position.
@param[out] *exit_time Exit time.
@param[out] *exit_x_position x-coordinate of the position at time exit_time.
@param[out] *exit_y_position y-coordinate of the position at time exit_time. 
@param[out] *side Side at which the exit takes place.
	-0 bottom
	-1 right
	-2 up
	-3 left

@retval 0 failure, 1 success.

@pre \e SQ_half_width and \e SQ_half_height shall be correctly defined and are not checked. 
*/

int r_exit_time_position_rectangle(gsl_rng *rng,
	double half_width, double half_height,
	double SQ_half_width, double SQ_half_height,
	double xpos, double ypos,
	double *exit_time, double *exit_x_position, double *exit_y_position,
	int *side)
{
	int epsilon_x,epsilon_y;
	double tmp;
	double time_x_exit; 
	double norm_time_x_exit; 
	double time_y_exit; 
	double norm_time_y_exit; 
	double norm_xpos;
	double norm_ypos;
	double tmppos;
	int counter=0;
	int swap;


	// We swap the coordinates because the probability
	// to leave by the closest side is the highest. 
	// This gives right to a faster simulation in average.
	if ((swap=((half_width-fabs(xpos))>(half_height-fabs(ypos)))))
	{
	    tmp=half_width;
	    half_width=half_height;
	    half_height=tmp;
	    tmp=SQ_half_width;
	    SQ_half_width=SQ_half_height;
	    SQ_half_height=tmp;
	    tmp=xpos;
	    xpos=-ypos;
	    ypos=tmp;
	}
	// Normalization of the starting point.
	norm_xpos=xpos/half_width;
	// The starting point shall not be too close from the 
	// endpoints (for tabulated values)
	if (fabs(norm_xpos)>0.999)
		norm_xpos=0.999*GSL_SIGN(norm_xpos);
	norm_ypos=ypos/half_height;
	// The starting point shall not be too close from the 
	// endpoints (for tabulated values)
	if (fabs(norm_ypos)>0.999)
		norm_ypos=0.999*GSL_SIGN(norm_ypos);
	// Compute the probability that the first component leaves
	// by the right endpoint.
	tmp=(1.0+norm_xpos)/2.0; 
	epsilon_x=2*(gsl_rng_uniform(rng)<tmp)-1;
	// Exit time of the first component.
	time_x_exit=SQ_half_width*r_exit_time_right(rng,epsilon_x*norm_xpos);
	// Normalization of the exit time of the first component.
	norm_time_x_exit=time_x_exit/SQ_half_height;
	// Probability that the 2nd component leave the interval 
	// [-half_height,half_height] before time_x_exit.
	tmp=p_exit_time(norm_time_x_exit,norm_ypos);
	if (gsl_rng_uniform(rng)<tmp)
		{
		// If: The particle leaves the rectangle by the sides 0 or 2.
		// Decide if the particle leave by side 2 instead of side 0. 
		tmp=p_exit_time_right(norm_time_x_exit,norm_ypos)*(1+norm_ypos)/(2*p_exit_time(norm_time_x_exit,norm_ypos));
		epsilon_y=2*(gsl_rng_uniform(rng)<tmp)-1;
		// Computation of the normalized exit time.
		norm_time_y_exit=r_exit_time_right_gets(rng,norm_time_x_exit,epsilon_y*norm_ypos);	
		// The real exit time.
		time_y_exit=SQ_half_height*norm_time_y_exit;
		// Compute the exit position on side 1 or 3 (depending on epsilon_x).
		// This step is numerically costly.
		do
			{
			tmp=r_pos_right_gete(rng,time_y_exit/SQ_half_width,time_x_exit/SQ_half_width,epsilon_x*norm_xpos);
			counter++;
			}
		// ERROR_r_pos_gne is the value for the error
		// of r_pos_right_gete.
		while (tmp==ERROR_r_pos_gne && counter<40);
		// In case of failure, return a failure value.
		if (counter==40)
			return 0;
		tmppos=half_width*tmp*epsilon_x;
		*side=1+epsilon_y; //alternative (*epsilon_y==1 ? 2 : 0)
		*exit_x_position=tmppos;
		*exit_y_position=epsilon_y*half_height;
		*exit_time=time_y_exit;
		if (swap)
			{
			*exit_x_position=*exit_y_position;
			*exit_y_position=-tmppos;
			*side=(*side+3) % 4; //alternative (*side==0 ? 3 : 1)
			}
		}
		else
		{
		// on sort par la droite ou la gauche
		// If: the particle leaves by sides 1 or 3.
		tmppos=half_height*r_pos_gne(rng,norm_time_x_exit,norm_ypos);
		*side=2-epsilon_x; // *side takes the values 3 ou 1.
		*exit_x_position=epsilon_x*half_width;
		*exit_y_position=tmppos;
		*exit_time=time_x_exit;
		// If the coordinates have been swapped, 
		// then put then in right order.
		if (swap)
			{
			*exit_y_position=-*exit_x_position;
			*exit_x_position=tmppos;
			*side=*side-1;
			}
		}
	return 1;
}

/** \brief Simulate the exit time and position from a time-space
rectangle for the Brownian motion.

@param *rng Random number generator.
@param half_width  Half of the width of the rectangle.
@param half_height Half of the height of the rectangle.
@param SQ_half_width square of half_width (not checked).
@param SQ_half_height square of half_height (not checked).
@param final_time Time at which is position is considered [in]
@param xpos x-coordinate of the initial position.
@param ypos y-coordinate of the initial position.
@param[out] *exit_time Exit time.
@param[out] *exit_x_position x-coordinate of the position at time exit_time.
@param[out] *exit_y_position y-coordinate of the position at time exit_time. 
@param[out] *side Side at which the exit takes place.
	-0 bottom
	-1 right
	-2 top
	-3 left
	-4 final_time

@retval 0 failure, 1 success.

@pre \e SQ_half_width and \e SQ_half_height shall be correctly defined and are not checked. 
*/

int r_exit_time_position_space_time_rectangle(gsl_rng *rng,
	double half_width, double half_height,
	double SQ_half_width, double SQ_half_height,
	double final_time,
	double xpos, double ypos,
	double *exit_time, double *exit_x_position, double *exit_y_position,
	int *side)
{
	int epsilon_x,epsilon_y;
	double tmp;
	double time_x_exit; 
	double normx_time_x_exit, normy_time_x_exit, normy_time_y_exit, normx_time_y_exit;
	double time_y_exit; 
	double norm_xpos;
	double norm_ypos;
	double tmppos;
	double normx_final_time,normy_final_time;
	int counter=0;
	int swap;

	// We swap the coordinates because the probability
	// to leave by the closest side is the highest. 
	// This gives right to a faster simulation in average.
	if ((swap=((half_width-fabs(xpos))>(half_height-fabs(ypos)))))
	{
	    tmp=half_width;
	    half_width=half_height;
	    half_height=tmp;
	    tmp=SQ_half_width;
	    SQ_half_width=SQ_half_height;
	    SQ_half_height=tmp;
	    tmp=xpos;
	    xpos=-ypos;
	    ypos=tmp;
	}
	// Normalization of the starting point.
	norm_xpos=xpos/half_width;
	// The starting point shall not be too close from the 
	// endpoints (for tabulated values)
	if (fabs(norm_xpos)>0.999)
		norm_xpos=0.999*GSL_SIGN(norm_xpos);
	norm_ypos=ypos/half_height;
	// The starting point shall not be too close from the 
	// endpoints (for tabulated values)
	if (fabs(norm_ypos)>0.999)
		norm_ypos=0.999*GSL_SIGN(norm_ypos);
	normx_final_time=final_time/SQ_half_width;
	normy_final_time=final_time/SQ_half_height;
	// Probability that the first component leaves 
	// [-half_width,half_width] before final_time.
	tmp=p_exit_time(normx_final_time,norm_xpos);
	if (gsl_rng_uniform(rng)<tmp)
	    { // If: The first component leaves [-half_width,half_width]
		// before final_time.
		// Computation of the exit position of the first component.
		tmp=p_exit_time_right(normx_final_time,norm_xpos)*(1+norm_xpos)/(2*p_exit_time(normx_final_time,norm_xpos));
		epsilon_x=2*(gsl_rng_uniform(rng)<tmp)-1;
		// Computation of the exit time of the first component.
		normx_time_x_exit=r_exit_time_right_gets(rng,normx_final_time,epsilon_x*norm_xpos);	
		// The real exit time.
		time_x_exit=SQ_half_width*normx_time_x_exit;	    
		// Normalization of the exit time
		normy_time_x_exit=time_x_exit/SQ_half_height;
		tmp=p_exit_time(normy_time_x_exit,norm_ypos);
		if (gsl_rng_uniform(rng)<tmp)
		    {
		    // If: The particle leaves the rectangle by the sides 0 or 2.
		    // Decide if the particle leave by side 2 instead of side 0. 
		    tmp=p_exit_time_right(normy_time_x_exit,norm_ypos)*(1+norm_ypos)/(2*p_exit_time(normy_time_x_exit,norm_ypos));
		    epsilon_y=2*(gsl_rng_uniform(rng)<tmp)-1;
		    // Computation of the normalized exit time.
		    normy_time_y_exit=r_exit_time_right_gets(rng,normy_time_x_exit,epsilon_y*norm_ypos);	
		    // The real exit time.
		    time_y_exit=SQ_half_height*normy_time_y_exit;
		    normx_time_y_exit=time_y_exit/SQ_half_width;
		    // Compute the exit position on side 1 or 3 (depending on epsilon_x).
		    // This step is numerically costly.
		    do
			    {
			    tmp=r_pos_right_gete(rng,normx_time_y_exit,normx_time_x_exit,epsilon_x*norm_xpos);
			    counter++;
			    }
		    // ERROR_r_pos_gne is the value for the error
		    // of r_pos_right_gete.
		    while (tmp==ERROR_r_pos_gne && counter<40);
		    // In case of failure, return a failure value.
		    if (counter==40)
			    return 0;
		    tmppos=half_width*tmp*epsilon_x;
		    *side=1+epsilon_y; //alternative (*epsilon_y==1 ? 2 : 0)
		    *exit_x_position=tmppos;
		    *exit_y_position=epsilon_y*half_height;
		    *exit_time=time_y_exit;
		    if (swap)
			    {
			    *exit_x_position=*exit_y_position;
			    *exit_y_position=-tmppos;
			    *side=(*side+3) % 4; //alternative (*side==0 ? 3 : 1)
			    }
		    }
		else
		    {
		    // on sort par la droite ou la gauche
		    // If: the particle leaves by sides 1 or 3.
		    tmppos=half_height*r_pos_gne(rng,normy_time_x_exit,norm_ypos);
		    *side=2-epsilon_x; // *side takes the values 3 ou 1.
		    *exit_x_position=epsilon_x*half_width;
		    *exit_y_position=tmppos;
		    *exit_time=time_x_exit;
		    // If the coordinates have been swapped, 
		    // then put then in right order.
		    if (swap)
			    {
			    *exit_y_position=-*exit_x_position;
			    *exit_x_position=tmppos;
			    *side=*side-1;
			    }
		    }
	    }
	else
	    { // Else: The first component leaves [-half_width,half_width] after final_time
	    tmp=p_exit_time(normy_final_time,norm_ypos);
	    if (gsl_rng_uniform(rng)<tmp)
		{
		tmp=p_exit_time_right(normy_final_time,norm_ypos)*(1+norm_ypos)/(2*p_exit_time(normy_final_time,norm_ypos));
		epsilon_y=2*(gsl_rng_uniform(rng)<tmp)-1;
		// Computation of the normalized exit time.
		normy_time_y_exit=r_exit_time_right_gets(rng,normy_final_time,epsilon_y*norm_ypos);	
		// The real exit time.
		time_y_exit=SQ_half_height*normy_time_y_exit;
		normx_time_y_exit=time_y_exit/SQ_half_width;
		// The position of the first component.
		tmppos=half_width*r_pos_getg(rng,normx_time_y_exit,normx_final_time,norm_xpos);
		*side=1+epsilon_y; //alternative (*epsilon_y==1 ? 2 : 0)
		*exit_x_position=tmppos;
		*exit_y_position=epsilon_y*half_height;
		*exit_time=time_y_exit;
		if (swap)
			{
			*exit_x_position=*exit_y_position;
			*exit_y_position=-tmppos;
			*side=(*side+3) % 4; //alternative (*side==0 ? 3 : 1)
			}
		}
	    else
		{
		// Else: The second component leaves [-half_height,half_height] after final_time. Returns the position of the particle at time final_time.
		*exit_time=final_time;
		*side=4;
		if (swap)
		    {
		    *exit_x_position=r_pos_gne(rng,normy_final_time,norm_ypos)*half_height;

		    *exit_y_position=-r_pos_gne(rng,normx_final_time,norm_xpos)*half_width;
		    }	
		else
		    {
		    *exit_x_position=r_pos_gne(rng,normx_final_time,norm_xpos)*half_width;
		    *exit_y_position=r_pos_gne(rng,normy_final_time,norm_ypos)*half_height;
		    }
		}
	    }
	return 1;
}

/** \brief Simulate the exit time and position from a rectangle
 * using the random walk on squares.
 *
 * \param[in] rng Random number generator.
 * \param[in] half_width Half of the width of the rectangle.
 * \param[in] half_height Half the height of the rectangle.
 * \param[in] final_time The final time.
 * \param[in] xpos x-coordinate of the initial position.
 * \param[in] ypos y-coordinate of the initial position.
 * \param[out] exit_time exit time.
 * \param[out] x x-coordinate of the exit position.
 * \param[out] y y-coordinate of the exit position.
 * \param[out] side side of the exit
	-0 bottom
	-1 right
	-2 up
	-3 left
	-4 final time
 *
 * This method aims to be faster than \a r_exit_time_position_rectangle, as \a r_exit_time_position_square uses tabulated values.
 */

int r_exit_time_position_space_time_rectangle_rws(gsl_rng *rng,
	double half_width, double half_height, double final_time,
	double xpos, double ypos,
	double *exit_time, double *x, double *y,
	int *side)
{
    double new_time,max_time;
    double new_x,new_y;
    double dist_x,dist_y,dist,sq_dist;
    typedef enum SIDE {BOT=0,RIGHT=1,TOP=2,LEFT=3} index_side;
    // Array of booleans regarding the sides by which the particle may exit.
    int side_possible[4];
#ifdef ASSERT_MODE
    assert(fabs(xpos)<half_width);
    assert(fabs(ypos)<half_height);
#endif
    *side=5; // means no exit
    *x=xpos;
    *y=ypos;
    *exit_time=0.0;
    while (*side==5) {
	side_possible[BOT]=0;	
	side_possible[RIGHT]=0;	
	side_possible[TOP]=0;	
	side_possible[LEFT]=0;	
	if(*x>0.0)
	{
	   dist_x=half_width-*x;  
	   side_possible[RIGHT]=1;
	}
	else if(*x<0.0)
	{
	   dist_x=*x+half_width;  
	   side_possible[LEFT]=1;
	}
	else if(*x==0.0)
	{
	    dist_x=half_width;
	    side_possible[LEFT]=1;
	    side_possible[RIGHT]=1;
	}
	if(*y>0.0)
	{
	   dist_y=half_height-*y;  
	   side_possible[TOP]=1;
	}
	else if(*y<0.0)
	{
	   dist_y=*y+half_height;  
	   side_possible[BOT]=1;
	}
	else if(*y==0.0)
	{
	    dist_y=half_height;
	    side_possible[TOP]=1;
	    side_possible[BOT]=1;
	}
	if (dist_x<dist_y)
	{
	    side_possible[TOP]=0;
	    side_possible[BOT]=0; 
	    dist=dist_x;
	}
	else if (dist_x>dist_y)
	{
	    side_possible[LEFT]=0;
	    side_possible[RIGHT]=0; 
	    dist=dist_y;
	}
	else 
	{
	    dist=dist_x;
	}
#ifdef ASSERT_MODE
	assert(dist>0.0);
#endif
	sq_dist=dist*dist;
	max_time=(final_time-*exit_time)/sq_dist;
#ifdef ASSERT_MODE
	assert(r_exit_time_position_space_time_square(rng,max_time,&new_time,&new_x,&new_y));
#else
	r_exit_time_position_space_time_square(rng,max_time,&new_time,&new_x,&new_y);
#endif
	if (new_time==max_time)
	    {*side=4;}
	if (side_possible[RIGHT] && (new_x==1.0))
	    {*side=1;}
	if (side_possible[LEFT] && (new_x==-1.0))
	    {*side=3;}
	if (side_possible[TOP] && (new_y==1.0))
	    {*side=2;}
	if (side_possible[BOT] && (new_y==-1.0))
	    {*side=0;}
	*x+=new_x*dist;
	*y+=new_y*dist;
	*exit_time+=sq_dist*new_time;
	// Security, otherwise, the rounding errors may cause some difficulties
	if((*exit_time>(final_time-1e-8)) && (*side==5))
	{
	    *side=4;
	}
    };
    return 1;
}

/*@}*/
