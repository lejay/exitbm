/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011,2012
**********************************************************************/
/** @file exitbm.c 

@brief Functions related to the exit time
and the position of a Brownian motion in an interval.

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/

/**\mainpage exitbm Documentation

\section section_intro Introduction

Computes the densities and distribution functions
related to the exit time and position from an interval
for the Brownian motion. Allows us to simulate some random
variables with these distributions.

Let \f$B\f$ be a one-dimensional Brownian motion, and \f$I=(-1,1)\f$.
Denote by \f$\tau\f$ its first exit time from \f$I\f$, id est
\f[
\tau=\inf\{t>0\,;\,|B_t|=1\}=\inf\{t>0\,;\,B_t\not\in[-1,1]\}
\f]
and \f$\mathbb{P}_x\f$
its distribution when its starting point is \f$x\f$.

The mathematical documentation of this library
may be found in 
<em>\p exitbm: a library for simulating Brownian motion's exit times and positions from simple domains</em>, Antoine Lejay, Technical report, INRIA, RT-0402 (2011)
downloadable on <http://hal.inria.fr/inria-00561409>.

\section section_dependencies Dependencies 

This library uses the Gnu Scientific Library (GSL).

\section section_simulation Initialisation of random variables

For random variates simulation, the variable 
\a rng shall be defined the following way
(by using the GSL random number generation):
@code
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
@endcode
and is initialize by 
@code
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);
@endcode
See the GSL manual <http://www.gnu.org/software/gsl/manual/> for
further information.

@author Antoine Lejay

@version 2.3

@date April 20, 2012
*/

/** @defgroup mathfunctions Mathematical functions 
 *
 * @brief Generic mathematical functions.
 *
 * */

/** @defgroup posBM Position of the Brownian motion 

@brief Functions related to the position at a given time of the Brownian
motion killed when it exits from \f$[-1,1]\f$.
*/

/** @defgroup exittime Exit time of the Brownian motion

@brief Functions related to the first exit time from \f$[-1,1]\f$
for the Brownian motion.
*/

/** Header files */
#include <exitbm.h>
#include <assert.h>



/** @addtogroup posBM */
//@{
/** \brief Maximum number of iterations in \p r_pos_gne_check */
#define MAX_ITER_r_pos_gne 10 
/** \brief Maximum number of iterations in \p r_pos_right_gete */
#define MAX_ITER_r_pos_right_gete 40 
//@}



/**
@ingroup mathfunctions
@brief The exponential function is redefined to avoid some underflow/overflow error.

Maybe, there are some clever way to proceed.
*/
double secure_exp(double x)
{
	if (x>700)
		{
		fprintf(stderr,"Overflow on exp %f\n",x);
		return(GSL_POSINF);
		}
	return (x < -120.0 ? 0.0 : gsl_sf_exp(x));
}

/** @addtogroup posBM */
/*@{*/
/** \brief Tabulated values for a first guess for invertion 
the distribution function of the position of the Brownian 
motion given no exit.

\see q_pos_gne_guess  */
double pos_gne_inverse_first_term[] = {
1.000000, //-1.000000
0.909893, //-0.990000
0.872463, //-0.980000
0.843668, //-0.970000
0.819331, //-0.960000
0.797835, //-0.950000
0.778351, //-0.940000
0.760387, //-0.930000
0.743623, //-0.920000
0.727837, //-0.910000
0.712867, //-0.900000
0.698592, //-0.890000
0.684915, //-0.880000
0.671763, //-0.870000
0.659073, //-0.860000
0.646796, //-0.850000
0.634890, //-0.840000
0.623319, //-0.830000
0.612053, //-0.820000
0.601066, //-0.810000
0.590334, //-0.800000
0.579839, //-0.790000
0.569562, //-0.780000
0.559488, //-0.770000
0.549602, //-0.760000
0.539893, //-0.750000
0.530349, //-0.740000
0.520960, //-0.730000
0.511716, //-0.720000
0.502610, //-0.710000
0.493633, //-0.700000
0.484779, //-0.690000
0.476040, //-0.680000
0.467412, //-0.670000
0.458887, //-0.660000
0.450462, //-0.650000
0.442131, //-0.640000
0.433890, //-0.630000
0.425735, //-0.620000
0.417661, //-0.610000
0.409666, //-0.600000
0.401745, //-0.590000
0.393895, //-0.580000
0.386114, //-0.570000
0.378398, //-0.560000
0.370745, //-0.550000
0.363152, //-0.540000
0.355616, //-0.530000
0.348136, //-0.520000
0.340709, //-0.510000
0.333333, //-0.500000
0.326006, //-0.490000
0.318727, //-0.480000
0.311492, //-0.470000
0.304301, //-0.460000
0.297152, //-0.450000
0.290043, //-0.440000
0.282973, //-0.430000
0.275940, //-0.420000
0.268943, //-0.410000
0.261980, //-0.400000
0.255050, //-0.390000
0.248152, //-0.380000
0.241285, //-0.370000
0.234447, //-0.360000
0.227637, //-0.350000
0.220854, //-0.340000
0.214098, //-0.330000
0.207366, //-0.320000
0.200658, //-0.310000
0.193973, //-0.300000
0.187311, //-0.290000
0.180669, //-0.280000
0.174047, //-0.270000
0.167445, //-0.260000
0.160861, //-0.250000
0.154295, //-0.240000
0.147745, //-0.230000
0.141211, //-0.220000
0.134693, //-0.210000
0.128188, //-0.200000
0.121698, //-0.190000
0.115220, //-0.180000
0.108754, //-0.170000
0.102299, //-0.160000
0.095855, //-0.150000
0.089421, //-0.140000
0.082995, //-0.130000
0.076579, //-0.120000
0.070170, //-0.110000
0.063769, //-0.100000
0.057373, //-0.090000
0.050984, //-0.080000
0.044600, //-0.070000
0.038220, //-0.060000
0.031844, //-0.050000
0.025472, //-0.040000
0.019101, //-0.030000
0.012733, //-0.020000
0.006366, //-0.010000
0.000000, //0.000000
-0.006366, //0.010000
-0.012733, //0.020000
-0.019101, //0.030000
-0.025472, //0.040000
-0.031844, //0.050000
-0.038220, //0.060000
-0.044600, //0.070000
-0.050984, //0.080000
-0.057373, //0.090000
-0.063769, //0.100000
-0.070170, //0.110000
-0.076579, //0.120000
-0.082995, //0.130000
-0.089421, //0.140000
-0.095855, //0.150000
-0.102299, //0.160000
-0.108754, //0.170000
-0.115220, //0.180000
-0.121698, //0.190000
-0.128188, //0.200000
-0.134693, //0.210000
-0.141211, //0.220000
-0.147745, //0.230000
-0.154295, //0.240000
-0.160861, //0.250000
-0.167445, //0.260000
-0.174047, //0.270000
-0.180669, //0.280000
-0.187311, //0.290000
-0.193973, //0.300000
-0.200658, //0.310000
-0.207366, //0.320000
-0.214098, //0.330000
-0.220854, //0.340000
-0.227637, //0.350000
-0.234447, //0.360000
-0.241285, //0.370000
-0.248152, //0.380000
-0.255050, //0.390000
-0.261980, //0.400000
-0.268943, //0.410000
-0.275940, //0.420000
-0.282973, //0.430000
-0.290043, //0.440000
-0.297152, //0.450000
-0.304301, //0.460000
-0.311492, //0.470000
-0.318727, //0.480000
-0.326006, //0.490000
-0.333333, //0.500000
-0.340709, //0.510000
-0.348136, //0.520000
-0.355616, //0.530000
-0.363152, //0.540000
-0.370745, //0.550000
-0.378398, //0.560000
-0.386114, //0.570000
-0.393895, //0.580000
-0.401745, //0.590000
-0.409666, //0.600000
-0.417661, //0.610000
-0.425735, //0.620000
-0.433890, //0.630000
-0.442131, //0.640000
-0.450462, //0.650000
-0.458887, //0.660000
-0.467412, //0.670000
-0.476040, //0.680000
-0.484779, //0.690000
-0.493633, //0.700000
-0.502610, //0.710000
-0.511716, //0.720000
-0.520960, //0.730000
-0.530349, //0.740000
-0.539893, //0.750000
-0.549602, //0.760000
-0.559488, //0.770000
-0.569562, //0.780000
-0.579839, //0.790000
-0.590334, //0.800000
-0.601066, //0.810000
-0.612053, //0.820000
-0.623319, //0.830000
-0.634890, //0.840000
-0.646796, //0.850000
-0.659073, //0.860000
-0.671763, //0.870000
-0.684915, //0.880000
-0.698592, //0.890000
-0.712867, //0.900000
-0.727837, //0.910000
-0.743623, //0.920000
-0.760387, //0.930000
-0.778351, //0.940000
-0.797835, //0.950000
-0.819331, //0.960000
-0.843668, //0.970000
-0.872463, //0.980000
-0.909893, //0.990000
-1.000000, //1.000000
};
/*@}*/


/***********************************************************************
TEMPS DE SORTIE 
***********************************************************************/
/** @addtogroup exittime */
/*@{*/

/**
\brief Distribution function of the exit time of \f$[-1,1]\f$ for the 
Brownian motion for small time.

The expression is obtained
by the method of images.

@param t Time.
@param x Initial position of the Brownian motion.
*/
double p_exit_time_small(double t, double x)
{
	int N=6;
	double sqrt2t;
	double xminus1=x-1;
	double xplus1=x+1;
	double result_p;
	int n;
	sqrt2t=sqrt(2.0*t);
	result_p=ERFC(xminus1/sqrt2t)-ERFC(xplus1/sqrt2t);
	for (n=4;n<=4*N;n+=4)
		{
		result_p+=ERFC((xminus1-n)/sqrt2t);
		result_p-=ERFC((xplus1-n)/sqrt2t);
		result_p+=ERFC((xminus1+n)/sqrt2t);
		result_p-=ERFC((xplus1+n)/sqrt2t);
		}
	return 2.0-result_p;
}


/**
\brief Distribution function of the exit time of \f$[-1,1]\f$ for the 
Brownian motion for large time.

The expression is obtained by the spectral decomposition.

@param t Time.
@param x Initial position of the Brownian motion.
*/

double p_exit_time_large(double t, double x)
{
	double result_p=0.0;
	double tmp_df;
	double x_times_pi_over_2=x*M_PI_2;
	double tmp1,tmp4;
	
	tmp4=tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	result_p=tmp1*COS(x_times_pi_over_2);
	tmp4*=gsl_pow_8(tmp1);	
	tmp_df=tmp4*COS(3*x_times_pi_over_2);
	result_p-=tmp_df/3;
	return 1.0-M_4_OVER_PI*result_p;
}


/**
\brief Distribution function of the exit time of \f$[-1,1]\f$ for the 
Brownian motion. 

Call either \p p_exit_time_large() or 
\p p_exit_time_small().

@param t Time.
@param x Initial position of the Brownian motion.
*/

double p_exit_time(double t, double x)
{
   if (t<2.0)
	   return  p_exit_time_small(t,x);
   else
	   return p_exit_time_large(t,x);
}

/**
\brief Density of the exit time of \f$[-1,1]\f$ for the 
Brownian motion for small time.

The expression is obtained by the method of images.

@param t Time.
@param x Initial position of the Brownian motion.
*/
double d_exit_time_small(double t, double x)
{
	int N=6;
	double twot=2*t;
	double xminus1=x-1;
	double xplus1=x+1;
	double xplus1minus4n;
	double xplus1plus4n;
	double xminus1minus4n;
	double xminus1plus4n;
	double result_d;
	int n;
	result_d=xplus1*EXP(-POW2(xplus1)/twot);
	result_d-=xminus1*EXP(-POW2(xminus1)/twot);
	for (n=4;n<=4*N;n+=4)
		{
		xplus1minus4n=xplus1-n;
		xplus1plus4n=xplus1+n;
		xminus1minus4n=xminus1-n;
		xminus1plus4n=xminus1+n;
		result_d+=xplus1minus4n*EXP(-POW2(xplus1minus4n)/twot);
		result_d+=xplus1plus4n*EXP(-POW2(xplus1plus4n)/twot);
		result_d-=xminus1minus4n*EXP(-POW2(xminus1minus4n)/twot);
		result_d-=xminus1plus4n*EXP(-POW2(xminus1plus4n)/twot);
		}
	return result_d/(t*sqrt(M_PI*twot));
}

/**\brief 
Density of the exit time of \f$[-1,1]\f$ for the 
Brownian motion for large time. 

The expression is obtained by the spectral decomposition.

@param t Time.
@param x Initial position of the Brownian motion.
*/
double d_exit_time_large(double t, double x)
{
	double result_d=0.0;
	double x_times_pi_over_2=x*M_PI_2;
	double tmp1;

	tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	result_d=tmp1*COS(x_times_pi_over_2);
	result_d-=3*tmp1*gsl_pow_8(tmp1)*COS(3*x_times_pi_over_2);
	return result_d*M_PI_2;
}

/**\brief 
Density of the exit time of \f$[-1,1]\f$ for the 
Brownian motion.

Uses either \p d_exit_time_small() 
or \p d_exit_time_large().

@param t Time.
@param x Initial position of the Brownian motion.
*/

double d_exit_time(double t, double x)
{
   if (t<1.2)
	   return  d_exit_time_small(t,x);
   else
	   return d_exit_time_large(t,x);
}

/**\brief 
Simultaneous computation of the exit first
exit time from \f$[-1,1]\f$ for the Brownian motion,
for efficiency reason, for small time.

@param t Time.
@param x Initial point.
@param *value_p Value of the distribution function.
@param *value_d Value of the density.
*/
void pd_exit_time_small(double t, double x,
			double *value_p, double *value_d)
{
	int N=6;
	double sqrt2t;
	double twot=2*t;
	double xminus1=x-1;
	double xplus1=x+1;
	double xplus1minus4n;
	double xplus1plus4n;
	double xminus1minus4n;
	double xminus1plus4n;
	double result_d;
	double result_p;
	int n;
	sqrt2t=sqrt(2*t);
	result_p=ERFC(xminus1/sqrt2t)-ERFC(xplus1/sqrt2t);
	result_d=xplus1*EXP(-POW2(xplus1)/twot);
	result_d-=xminus1*EXP(-POW2(xminus1)/twot);
	for (n=4;n<=4*N;n+=4)
		{
		result_p+=ERFC((xminus1-n)/sqrt2t);
		result_p-=ERFC((xplus1-n)/sqrt2t);
		result_p+=ERFC((xminus1+n)/sqrt2t);
		result_p-=ERFC((xplus1+n)/sqrt2t);
		xplus1minus4n=xplus1-n;
		xplus1plus4n=xplus1+n;
		xminus1minus4n=xminus1-n;
		xminus1plus4n=xminus1+n;
		result_d+=xplus1minus4n*EXP(-POW2(xplus1minus4n)/twot);
		result_d+=xplus1plus4n*EXP(-POW2(xplus1plus4n)/twot);
		result_d-=xminus1minus4n*EXP(-POW2(xminus1minus4n)/twot);
		result_d-=xminus1plus4n*EXP(-POW2(xminus1plus4n)/twot);
		}
	*value_p=2-result_p;
	*value_d=result_d/(t*sqrt(M_PI*twot));
}

/**\brief 
Simultaneous computation of the exit first
exit time from \f$[-1,1]\f$ for the Brownian motion,
for efficiency reason, for large time.

@param t Time.
@param x Initial point.
@param[out] *value_p Value of the distribution function.
@param[out] *value_d Value of the density.
*/

void pd_exit_time_large(double t, double x, double *value_p, double *value_d)
{
	double result_p=0.0;
	double result_d=0.0;
	double tmp_df;
	double tmp_dty;
	double x_times_pi_over_2=x*M_PI_2;
	double tmp1,tmp3,tmp4;

	tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	tmp3=0.0;
	result_p=tmp1*COS(x_times_pi_over_2);
	result_d=result_p;
	tmp4=tmp1*gsl_pow_8(tmp1);
	tmp_df=tmp4*COS(3*x_times_pi_over_2);
	tmp_dty=3*tmp_df;
	tmp_df/=3;
	result_p-=tmp_df;
	result_d-=tmp_dty;
	*value_p=1.0-result_p*M_4_OVER_PI;
	*value_d=result_d*M_PI_2;
}


/**\brief 
Simultaneous computation of the exit first
exit time from \f$[-1,1]\f$ for the Brownian motion,
for efficiency reason.

Uses either 
\p pd_exit_time_small() or \p pd_exit_time_large().

@param t Time.
@param x Initial point.
@param *value_p Value of the distribution function.
@param *value_d Value of the density.
*/
void  pd_exit_time(double t, double x, 
		double *value_p, double *value_d)
{
   if (t<1.2)
	   pd_exit_time_small(t,x,value_p,value_d);
   else
	   pd_exit_time_large(t,x,value_p,value_d);
}

/** \brief Inverse function of the distribution function
of the exit time from \f$[-1,1]\f$ for the Brownian motion, 
with a bissection method.

This function is called
if the Newton algorithm does not succeed to converge.

@param value Target value.
@param x Initial point.
@param corr_error Instead of solving \f$F(u)/c=value\f$ for \f$ c>0\f$,
we compute \f$F^{-1}(c\times value)\f$.
The error is then multiplied by \p corr_error. 

\warning Note that \e value is not multiplied by \e corr_error.
*/
double q_exit_time_bissection
	(double value, double x, double corr_error)
{
	double t_min=1e-20;
	double t_max=20.0;
	double t_middle;
	double error=1e-20;
	double error_eval=1e-10;
	double eval_max,eval_min;
	double eval_middle;
	error_eval*=corr_error;
	eval_min=p_exit_time(t_min,x)-value;
	eval_max=p_exit_time(t_max,x)-value;
	if (eval_min>0.0 || eval_max<0.0)
		return ERROR_q_exit_time;
	do
		{
		t_middle=0.5*(t_max+t_min);
		eval_middle=p_exit_time(t_middle,x)-value;
		if (fabs(eval_middle)<error_eval)
			return t_middle;
		if (eval_middle<0.0)
			t_min=t_middle;
		else
			t_max=t_middle;
		}
	while (t_max-t_min>error);
	return t_middle;
}

/** \brief Inverse function of the distribution function
of the exit time from \f$[-1,1]\f$ for the Brownian motion, 
with a Newton method. 

This function calls
\p q_exit_time_bissection() when it does not succeed to converge.

@param value Target value.
@param x Initial point.
@param corr_error Instead of solving 
\f$F(u)/c=value\f$ for \f$ c>0\f$, we compute \f$F^{-1}(c\times value)\f$.
The error is then multiplied by \p corr_error.

@warning Note that \e value is not multiplied by \e corr_error.
*/
double q_exit_time(double value, double x, double corr_error)
{
	double t_init;
	double t_next;
	double error=1e-10;
	double t_min=1e-10;
	double t_max=1e3;
	double eval,eval_dty;
	int iter=0;
	int max_iter=30;
	unsigned int status=1;
	error=GSL_MIN(error*corr_error,1e-10);
	if(value<1e-5)
	    { return(0.0);}
	// Uses a first approximation of the solution
	// to reduce the number of iterations.
	t_init=(1.0-value)*M_PI_4/COS(M_PI_2*x);
	t_init=((t_init>1.0) ? 0.01 : -LOG(t_init)/M_PI_SQUARE_OVER_8);
	while (status)
	{
		iter++;
		pd_exit_time(t_init,x,&eval,&eval_dty);
		eval-=value;
		if (fabs(eval)<error) 
			break;
		t_next=t_init-eval/eval_dty;
		status=((t_next>t_min) && (t_next<t_max) && (iter<max_iter));
		t_init=t_next;
	}
	if (status) 
		return t_init;
	else
		return q_exit_time_bissection(value,x,corr_error);
}

/***********************************************************************
TEMPS DE SORTIE CONDITIONNE PAR LE POINT DE SORTIE
***********************************************************************/

/**\brief  Density for small times of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit
position is at point 1. 

@param t Time.
@param x Initial position of the Brownian motion.
*/

double d_exit_time_right_small(double t,double x)
{
	int N=5;
	double twot=2*t;
	double xminus1=x-1;
	double xminus1minus4n;
	double xminus1plus4n;
	double result_d;
	int n;

	result_d=-xminus1*EXP(-POW2(xminus1)/twot);
	for (n=4;n<=4*N;n+=4)
		{
		xminus1minus4n=xminus1-n;
		xminus1plus4n=xminus1+n;
		result_d-=xminus1minus4n*EXP(-POW2(xminus1minus4n)/twot);
		result_d-=xminus1plus4n*EXP(-POW2(xminus1plus4n)/twot);
		}
	return 2*result_d/(t*sqrt(M_PI*twot)*(1+x));
}

/**\brief  Density for large times of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit
position is at point 1. 

\warning This expression gives a rough approximation 
for \e x pretty close to -1 (distance of order \f$10^{-5}\f$ or less).

@param t Time.
@param x Initial position of the Brownian motion.

*/
double d_exit_time_right_large(double t,double x)
{
	double xplus1=x+1;
	double result_d=0.0;
	double tmp1,tmp2,tmp3,tmp4;

	tmp3=tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	tmp4=tmp2=xplus1*M_PI_2;
	result_d=tmp1*SIN(tmp4);

	tmp3*=gsl_pow_3(tmp1); 
	tmp4+=tmp2;
	result_d-=2*tmp3*SIN(tmp4);

	tmp3*=gsl_pow_5(tmp1); 
	tmp4+=tmp2;
	result_d+=3*tmp3*SIN(tmp4);
	return M_PI_2*result_d/xplus1;
}

/**\brief Density of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit
position is at point 1. 

Uses either \p d_exit_time_right_small() or \p d_exit_time_right_large().

@param t Time.
@param x Initial position of the Brownian motion.
*/

double d_exit_time_right(double t, double x)
{
   if (t<2.0)
	   return  d_exit_time_right_small(t,x);
   else
	   return d_exit_time_right_large(t,x);
}


/**\brief Distribution function for small times of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

\warning This expression gives a rough approximation 
for \e x pretty close to -1 (distance of order \f$10^{-5}\f$ or less).

@param t Time.
@param x Initial position of the Brownian motion.
*/
double p_exit_time_right_small(double t,double x)
{
	int N=5;
	double sqrt2t;
	double xminus1=x-1;
	double result_p;
	int n;
	sqrt2t=sqrt(2*t);
	result_p=-2+ERFC(xminus1/sqrt2t);
	for (n=4;n<=4*N;n+=4)
		{
		result_p+=-1+ERFC((xminus1-n)/sqrt2t);
		result_p+=-1+ERFC((xminus1+n)/sqrt2t);
		}
	return -2*result_p/(1+x);
}


/**\brief Distribution function for large times of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

\warning This expression gives a rough approximation 
for \e x pretty close to -1.

@param t Time.
@param x Initial position of the Brownian motion.
*/
double p_exit_time_right_large(double t,double x)
{
	double xplus1=x+1;
	double result_p=0.0;
	double tmp1,tmp2,tmp3,tmp4;

	tmp3=tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	tmp4=tmp2=xplus1*M_PI_2;
	result_p=tmp1*SIN(tmp4);

	tmp3*=gsl_pow_3(tmp1); 
	tmp4+=tmp2;
	result_p-=tmp3*SIN(tmp4)/2; // TODO verifier ces termes

	tmp3*=gsl_pow_5(tmp1); 
	tmp4+=tmp2;
	result_p+=tmp3*SIN(tmp4)/3;

	return 1.0-M_4_OVER_PI*result_p/xplus1;
}

/**\brief  Distribution function of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

Uses either \p p_exit_time_right_small() or \p p_exit_time_right_large().

@param t Time.
@param x Initial position of the Brownian motion.
*/
double p_exit_time_right(double t, double x)
{
   if (t<2.0)
	   return  p_exit_time_right_small(t,x);
   else
	   return p_exit_time_right_large(t,x);
}

/**\brief Simultaneous computation of the distribution function
and the density for small time of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

This is more efficient than computing both functions separately
when needed.

@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_p value of the distribution function.
@param[out] *value_d Value of the density.
*/
void pd_exit_time_right_small(double t, double x,
	double *value_p, double *value_d)
{
	int N=5;
	double twot=2*t;
	double sqrt2t=sqrt(twot);
	double xminus1=x-1;
	double xminus1minus4n;
	double xminus1plus4n;
	double result_d;
	double result_p;
	int n;
	result_d=-xminus1*EXP(-POW2(xminus1)/twot);
	result_p=-2+ERFC(xminus1/sqrt2t);
	for (n=4;n<=4*N;n+=4)
		{
		xminus1minus4n=xminus1-n;
		xminus1plus4n=xminus1+n;
		result_d-=xminus1minus4n*EXP(-POW2(xminus1minus4n)/twot);
		result_d-=xminus1plus4n*EXP(-POW2(xminus1plus4n)/twot);
		result_p+=-1+ERFC((xminus1-n)/sqrt2t);
		result_p+=-1+ERFC((xminus1+n)/sqrt2t);
		}
	*value_d=2*result_d/(t*sqrt(M_PI*twot)*(1+x));
	*value_p=-2*result_p/(1+x);
}

/**\brief  Simultaneous computation of the distribution function
and the density for large time of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

This is more efficient than computing the both functions separately
when needed.

@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_p value of the distribution function.
@param[out] *value_d Value of the density.
*/
void pd_exit_time_right_large(double t, double x,
	double *value_p, double *value_d)
{
	double xplus1=x+1;
	double result_d=0.0;
	double result_p=0.0;
	double tmp1,tmp2,tmp3,tmp4,tmp5;

	tmp3=tmp1=EXP(-M_PI_SQUARE_OVER_8*t);
	tmp4=tmp2=xplus1*M_PI_2;
	result_p=result_d=tmp1*SIN(tmp4);

	tmp3*=gsl_pow_3(tmp1); 
	tmp4+=tmp2;
	tmp5=tmp3*SIN(tmp4);
	result_d-=2*tmp5;
	result_p-=tmp5/2;

	tmp3*=gsl_pow_5(tmp1); 
	tmp4+=tmp2;
	tmp5=tmp3*SIN(tmp4);
	result_d+=3*tmp5;
	result_p+=tmp5/3;

	*value_d=M_PI_2*result_d/xplus1;
	*value_p=1.0-M_4_OVER_PI*result_p/xplus1;
}

/**\brief  Simultaneous computation of the distribution function
and the density for large time of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

This is more efficient than computing the both functions separately
when needed.
Call either \p pd_exit_time_right_small() or \p pd_exit_time_right_large().

@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_p value of the distribution function.
@param[out] *value_d Value of the density.
*/
void pd_exit_time_right(double t, double x,
	double *value_p, double *value_d)
{
	if (t<2.0) 
		pd_exit_time_right_small(t,x,value_p,value_d);
	else
		pd_exit_time_right_large(t,x,value_p,value_d);
}

/**\brief Inverse function of the repartition function 
of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

A bissection method is used, and this function is called 
when the Newton method does not succeed to converge.

@param value Target value.
@param x Initial point.
@param corr_error Instead of solving 
\f$F(u)/c=value\f$ for \f$ c>0\f$, we compute \f$F^{-1}(c\times value)\f$.
The error is then multiplied by \p corr_error.

@warning Note that \e value is not multiplied by \e corr_error.
*/
double q_exit_time_right_bissection
	(double value, double x, double corr_error)
{
	double t_min=1e-20;
	double t_max=20.0;
	double t_middle;
	double error=1e-20;
	double error_eval=1e-10;
	double eval_max,eval_min;
	double eval_middle;
	error_eval*=corr_error;
	eval_min=p_exit_time(t_min,x)-value;
	eval_max=p_exit_time(t_max,x)-value;
	if (eval_min>0.0 || eval_max<0.0)
		return ERROR_q_exit_time;
		do
		{
		t_middle=0.5*(t_max+t_min);
		eval_middle=p_exit_time_right(t_middle,x)-value;
		if (fabs(eval_middle)<error_eval)
			return t_middle;
		if (eval_middle<0.0)
			t_min=t_middle;
		else
			t_max=t_middle;
		}
	while (t_max-t_min>error);
	return t_middle;
}

/** \brief Inverse function of the repartition function 
of the exit time from \f$[-1,1]\f$ for the 
Brownian motion with the condition that the exit position is at point 1. 

A Newton method is used, and this function call
the bissection method when it does not succeed to converge.

@param value Target value.
@param x Initial point.
@param corr_error Instead of solving \f$F(u)/c=value\f$ for \f$ c>0\f$,
we compute \f$F^{-1}(c\times value)\f$.
The error is then multiplied by \p corr_error.

@warning Note that \e value is not multiplied by \e corr_error.
*/
double q_exit_time_right(double value, double x, double corr_error)
{
	double t_init=0.0;
	double t_next;
	double error=1e-10;
	double t_min=0.0001;
	double t_max=1000.0;
	double eval_p,eval_d;
	int iter=0;
	int max_iter=30;
	int status=1;
	double xplus1=x+1.0;

	error=error*corr_error;
	t_init=(1-value)*M_PI_4*xplus1/SIN(xplus1*M_PI_2);
	t_init=(t_init > 1.0 ? 0.01 : -LOG(t_init)/M_PI_SQUARE_OVER_8);
	while (status)
	{
		iter++;
		pd_exit_time_right(t_init,x,&eval_p,&eval_d);
		eval_p-=value;
		if (fabs(eval_p)<error) 
			break;
		t_next=t_init-eval_p/eval_d;
		status=(t_next>t_min && t_next<t_max) && (iter<max_iter);
		t_init=t_next;
	}
	if (status) 
		return t_init;
	else
		{
		// Use the bissection method if needed.
		return q_exit_time_right_bissection(value,x,corr_error);
		}
}

/*@}*/

/********************************************************************
POSITION DU BROWNIEN TUE
********************************************************************/

/** @addtogroup posBM */
/*@{*/

/** \brief Integral between -1 and \e u of the density of the position of 
the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$.

This formula used the expression
of the density given by the formula of images
is shall be used for small times.

@warning This is not a probability distribution function!

@param u Upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
*/
double id_pos_small(double u, double t, double x)
{
	int N=5;
	double sqrttwotimest=sqrt(2.0*t);
	double xplus1divsqrttwotimest=(x+1)/sqrttwotimest;
	double xplusuminus2divsqrttwotimest=(x+u-2)/sqrttwotimest;
	double xminus3divsqrttwotimest=(x-3)/sqrttwotimest;
	double xminusudivsqrttwotimest=(x-u)/sqrttwotimest;
	double result_id=0.0;
	double ndivsqrttwotimest;
	int n;
	result_id-=ERFC(xplus1divsqrttwotimest);
	result_id+=ERFC(xminusudivsqrttwotimest);
	result_id-=ERFC(xminus3divsqrttwotimest);
	result_id+=ERFC(xplusuminus2divsqrttwotimest);
	for (n=4;n<=4*N;n+=4)
		{
		ndivsqrttwotimest=n/sqrttwotimest;
		result_id-=ERFC(xplus1divsqrttwotimest-ndivsqrttwotimest);
		result_id+=ERFC(xminusudivsqrttwotimest-ndivsqrttwotimest);
		result_id-=ERFC(xminus3divsqrttwotimest-ndivsqrttwotimest);
		result_id+=ERFC(xplusuminus2divsqrttwotimest-ndivsqrttwotimest);
		result_id-=ERFC(xplus1divsqrttwotimest+ndivsqrttwotimest);
		result_id+=ERFC(xminusudivsqrttwotimest+ndivsqrttwotimest);
		result_id-=ERFC(xminus3divsqrttwotimest+ndivsqrttwotimest);
		result_id+=ERFC(xplusuminus2divsqrttwotimest+ndivsqrttwotimest);
		}
	return result_id/2.0;
}

/**\brief Integral between -1 and \e u of the density of the position of 
the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$.

This formula used the expression
of the density given by the spectral decomposition
is shall be used for large times.

@warning This is not a probability distribution function!

@param u Upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double id_pos_large(double u, double t, double x)
{
	int N=3;
	double sq_pi_over_8_times_t=M_PI_SQUARE_OVER_8*t;
	double xplusonetimespiover2=M_PI_2*(x+1.0);
	double uplusonetimespiover2=M_PI_2*(u+1.0);
	double result_id=0.0;
	double tmp_df=0.0;
	int n;
	for(n=1;n<=N;n++)
		{
		tmp_df=EXP(-POW2((double) n)*sq_pi_over_8_times_t);
		tmp_df*=SIN(n*xplusonetimespiover2);
		tmp_df*=(1-COS(n*uplusonetimespiover2));
		result_id+=tmp_df/n;
		}
	return result_id/M_PI_2;
}

/**\brief Integral between -1 and \e u of the density of the position of 
the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$. Uses eiher \p id_pos_small() 
or \p id_pos_large().

@warning This is not a probability distribution function!

@param u Upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double id_pos(double u, double t, double x)
{
	if (t< 2.0)
		return id_pos_small(u,t,x);
	else
		return id_pos_large(u,t,x);
}

/**\brief Density of the position of the Brownian motion starting at
\e x and killed when it exits from \f$[-1,1]\f$.

This formula uses the expression
of the density given by the method of images and
shall be used for small times.

@warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double d_pos_small_alt(double u, double t, double x)
{
	int N=5;
	double twotimest=2*t;
	int n;
	double xminusu=x-u;
	double xplusuminus2=x+u-2.0;
	double result_d;
	result_d=EXP(-POW2(xminusu)/twotimest);
	result_d-=EXP(-POW2(xplusuminus2)/twotimest);
	for (n=4;n<=4*N;n+=4)
		{
		result_d+=EXP(-POW2(xminusu-n)/twotimest);
		result_d-=EXP(-POW2(xplusuminus2-n)/twotimest);
		result_d+=EXP(-POW2(xminusu+n)/twotimest);
		result_d-=EXP(-POW2(xplusuminus2+n)/twotimest);
		}
	return result_d/sqrt(twotimest*M_PI);
}

/**\brief  Density of the position of the Brownian motion starting
at \e x and killed when 
it exits from \f$[-1,1]\f$.

This formula used the expression
of the density given by the method of images
is shall be used for small times.

This procedure is much more rapid than \p d_pos_small_alt()
(the ratio of execution time is of order 0.6).

\warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/
double d_pos_small(double u, double t, double x)
{
	int N=5;
	int n;
	double twotimest=2*t;
	double xminusu=x-u;
	double xplusuminus2=x+u-2.0;
	double xplusuminus2times4overt=xplusuminus2*4/t;
	double xminusutimes4overt=xminusu*4/t;
	double fourovert=4.0/t;
	double minuseightovert=-8.0/t;
	double tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7,tmp8;
	double result_d, result_d1=0.0, result_d2=0.0;

	if (xplusuminus2times4overt<-120 || xplusuminus2times4overt>120 ||
		xminusutimes4overt<-120 || xminusutimes4overt>120)
		return d_pos_small_alt(u,t,x);
	// avoids a too big or small values in the exponentials

	tmp1=EXP(-POW2(xminusu)/twotimest);
	tmp2=EXP(-POW2(xplusuminus2)/twotimest);
	tmp3=EXP(fourovert*xminusu);
	tmp4=EXP(fourovert*xplusuminus2);
	tmp5=EXP(minuseightovert);
	tmp6=1.0;
	tmp7=1.0;
	tmp8=1.0;	

	for (n=1;n<=N;n++)
		{
		tmp6*=tmp3;
		tmp7*=tmp4;
		tmp8=gsl_pow_int(tmp5,n*n);
		result_d1+=tmp1*(tmp8*tmp6+tmp8/tmp6);
		result_d2+=tmp2*(tmp8*tmp7+tmp8/tmp7);
		}
	result_d=tmp1-tmp2+result_d1-result_d2;
	return result_d/sqrt(twotimest*M_PI);
}

/**\brief  Density of the position of the Brownian motion starting at \e x
and killed when it exits from \f$[-1,1]\f$. 

This formula used the expression
of the density given by the spectral decomposition is shall be used for
large times.

\warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/
double d_pos_large(double u, double t, double x)
{
	int N=3;
	double sq_pi_over_8_times_t=M_PI_SQUARE_OVER_8*t;
	double xplusonetimespiover2=M_PI_2*(x+1.0);
	double uplusonetimespiover2=M_PI_2*(u+1.0);
	double result_d=0.0;
	double tmp_dty=0.0;
	int n;
	for(n=1;n<=N;n++)
		{
		tmp_dty=EXP(-POW2((double) n)*sq_pi_over_8_times_t);
		tmp_dty*=SIN(n*xplusonetimespiover2);
		tmp_dty*=SIN(n*uplusonetimespiover2);
		result_d+=tmp_dty;
		}
	return result_d;
}

/**\brief  Density of the position of the Brownian motion starting at
\e x and killed when it exits from \f$[-1,1]\f$.

This function uses either 
\p d_pos_small() or \p d_pos_large().

\warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/
double d_pos(double u, double t, double x)
{
	if (t<2.0)
		return d_pos_small(u,t,x);
	else
		return d_pos_large(u,t,x);
}

/**\brief  Simultaneous computation of small time of the density 
and the integral between -1 and \e u 
of the position of the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$. 

@warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed and upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_id value of the integral.
@param[out] *value_d value of the density.
*/

void idd_pos_small_alt(double u, double t, double x,
	double *value_id, double *value_d)
{
	int N=5;
	double twotimest=2*t;
	double sqrttwotimest=sqrt(twotimest);
	int n;
	double xplus1=x+1;
	double xminus3=x-3.0;
	double xminusu=x-u;
	double xplusuminus2=x+u-2.0;
	double xplus1divsqrttwotimest=xplus1/sqrttwotimest;
	double xplusuminus2divsqrttwotimest=xplusuminus2/sqrttwotimest;
	double xminus3divsqrttwotimest=xminus3/sqrttwotimest;
	double xminusudivsqrttwotimest=xminusu/sqrttwotimest;
	double ndivsqrttwotimest;
	double result_id=0.0;
	double result_d=0.0;
	result_d=EXP(-POW2(xminusu)/twotimest);
	result_d-=EXP(-POW2(xplusuminus2)/twotimest);
	result_id-=ERFC(xplus1divsqrttwotimest);
	result_id+=ERFC(xminusudivsqrttwotimest);
	result_id-=ERFC(xminus3divsqrttwotimest);
	result_id+=ERFC(xplusuminus2divsqrttwotimest);
	for (n=4;n<=4*N;n+=4)
	{
		ndivsqrttwotimest=n/sqrttwotimest;
		
		result_d+=EXP(-POW2(xminusu-n)/twotimest);
		result_d-=EXP(-POW2(xplusuminus2-n)/twotimest);
		result_d+=EXP(-POW2(xminusu+n)/twotimest);
		result_d-=EXP(-POW2(xplusuminus2+n)/twotimest);
			
		result_id-=ERFC(xplus1divsqrttwotimest-ndivsqrttwotimest);
		result_id+=ERFC(xminusudivsqrttwotimest-ndivsqrttwotimest);
		result_id-=ERFC(xminus3divsqrttwotimest-ndivsqrttwotimest);
		result_id+=ERFC(xplusuminus2divsqrttwotimest-ndivsqrttwotimest);
		result_id-=ERFC(xplus1divsqrttwotimest+ndivsqrttwotimest);
		result_id+=ERFC(xminusudivsqrttwotimest+ndivsqrttwotimest);
		result_id-=ERFC(xminus3divsqrttwotimest+ndivsqrttwotimest);
		result_id+=ERFC(xplusuminus2divsqrttwotimest+ndivsqrttwotimest);
	}
	*value_id=result_id/2.0;
	*value_d=result_d/sqrt(twotimest*M_PI);
}

/**\brief  Simultaneous computation for small time of the density 
and the integral between -1 and \e u 
of the position of the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$. 

This function goes a bit faster than \p idd_pos_small_alt().

\warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed and upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_id value of the integral.
@param[out] *value_d value of the density.
*/
void idd_pos_small(double u, double t, double x,
	double *value_id, double *value_d)
{
	int N=5;
	int n;
	double twotimest=2*t;
	double sqrttwotimest=sqrt(twotimest);
	double fouroversqrttwotimest=4.0/sqrttwotimest;
	double fourovert=4.0/t;
	double minuseightovert=-8.0/t;
	double xplus1=x+1;
	double xminus3=x-3.0;
	double xminusu=x-u;
	double xplusuminus2=x+u-2.0;
	double xplus1divsqrttwotimest=xplus1/sqrttwotimest;
	double xplusuminus2divsqrttwotimest=xplusuminus2/sqrttwotimest;
	double xminus3divsqrttwotimest=xminus3/sqrttwotimest;
	double xminusudivsqrttwotimest=xminusu/sqrttwotimest;
	double ndivsqrttwotimest;
	double result_id=0.0;
	double result_d=0.0;
	double result_d1=0.0,result_d2=0.0;
	double tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7,tmp8;

	double xplusuminus2times4overt=xplusuminus2*4/t;
	double xminusutimes4overt=xminusu*4/t;

	if (xplusuminus2times4overt<-120 || xplusuminus2times4overt>120 ||
		xminusutimes4overt<-120 || xminusutimes4overt>120)
		{
		idd_pos_small_alt(u,t,x,value_id,value_d);
		}
	else
	{
		tmp1=EXP(-POW2(xminusu)/twotimest);
		tmp2=EXP(-POW2(xplusuminus2)/twotimest);
		tmp3=EXP(fourovert*xminusu);
		tmp4=EXP(fourovert*xplusuminus2);
		tmp5=EXP(minuseightovert);
		tmp6=1.0;
		tmp7=1.0;
		tmp8=1.0;	

		result_id-=ERFC(xplus1divsqrttwotimest);
		result_id+=ERFC(xminusudivsqrttwotimest);
		result_id-=ERFC(xminus3divsqrttwotimest);
		result_id+=ERFC(xplusuminus2divsqrttwotimest);
		for (n=1;n<=N;n++)
		{
			ndivsqrttwotimest=n*fouroversqrttwotimest;
			
			tmp6*=tmp3;
			tmp7*=tmp4;
			tmp8=gsl_pow_int(tmp5,n*n);
			result_d1+=tmp1*(tmp8*tmp6+ tmp8/tmp6);
			result_d2+=tmp2*(tmp8*tmp7+ tmp8/tmp7);
				
			result_id-=ERFC(xplus1divsqrttwotimest-ndivsqrttwotimest);
			result_id+=ERFC(xminusudivsqrttwotimest-ndivsqrttwotimest);
			result_id-=ERFC(xminus3divsqrttwotimest-ndivsqrttwotimest);
			result_id+=ERFC(xplusuminus2divsqrttwotimest-ndivsqrttwotimest);
			result_id-=ERFC(xplus1divsqrttwotimest+ndivsqrttwotimest);
			result_id+=ERFC(xminusudivsqrttwotimest+ndivsqrttwotimest);
			result_id-=ERFC(xminus3divsqrttwotimest+ndivsqrttwotimest);
			result_id+=ERFC(xplusuminus2divsqrttwotimest+ndivsqrttwotimest);
		}
		*value_id=result_id/2.0;
		result_d=tmp1-tmp2+result_d1-result_d2;
		*value_d=result_d/sqrt(twotimest*M_PI);
		}
}

/**\brief  Simultaneous computation for large time of the density 
and the integral between -1 and \e u 
of the position of the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$. 

\warning The total mass of this density is not equal to 1!

@param u Point at which is density is computed and upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_id value of the integral.
@param[out] *value_d value of the density.
*/

void idd_pos_large(double u, double t, double x, 
	double *value_id, double *value_d)
{
	int N=5;
	double sq_pi_over_8_times_t=M_PI_SQUARE_OVER_8*t;
	double xplusonetimespiover2=M_PI_2*(x+1.0);
	double uplusonetimespiover2=M_PI_2*(u+1.0);
	double result_id=0.0;
	double tmp_id=0.0;
	double result_d=0.0;
	double tmp_dty=0.0;
	int n;
	for(n=1;n<=N;n++)
	{
		tmp_id=EXP(-POW2((double) n)*sq_pi_over_8_times_t);
		tmp_id*=SIN(n*xplusonetimespiover2);
		tmp_id*=(1-COS(n*uplusonetimespiover2));
		result_id+=tmp_id/n;
			
		tmp_dty=EXP(-POW2((double) n)*sq_pi_over_8_times_t);
		tmp_dty*=SIN(n*xplusonetimespiover2);
		tmp_dty*=SIN(n*uplusonetimespiover2);
		result_d+=tmp_dty;
	}
	*value_id=result_id/M_PI_2;
	*value_d=result_d;
}

/**\brief Simultaneous computation of the density at \e y
and the integral between -1 and \e y 
of the position of the Brownian motion starting at \e x and killed when 
it exits from \f$[-1,1]\f$. 
This function uses either \p idd_pos_small() or \p idd_pos_large().

\warning The total mass of this density is not equal to 1!

@param y Point at which is density is computed and upper bound in the integral.
@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_id value of the integral.
@param[out] *value_d value of the density.
*/

void idd_pos(double y, double t, double x, 
	double *value_id, double *value_d)
{
	if (t<2.0)
		idd_pos_small(y,t,x,value_id,value_d);
	else
		idd_pos_large(y,t,x,value_id,value_d);
}

/**\brief Distribution function of the position of the Brownian motion starting at
\e x and conditioned not to have left \f$[-1,1]\f$ before the time \e t. 

@param y Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double p_pos_gne(double y, double t, double x)
{
	return id_pos(y,t,x)/(1-p_exit_time(t,x));
}

/**\brief  Density of the position of the Brownian motion starting at
\e x and conditioned not to have left \f$[-1,1]\f$ before the time \e t. 

@param y Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double d_pos_gne(double y, double t, double x)
{
	return d_pos(y,t,x)/(1-p_exit_time(t,x));
}

/**\brief  Simultaneous computation of the distribution function and 
the density of the position of the Brownian motion starting at
\e x and conditioned not to have left \f$[-1,1]\f$ before the time \e t. 

@param y Point at which is density is computed.
@param t Time.
@param x Initial position of the Brownian motion.
@param[out] *value_p Value of the distribution function.
@param[out] *value_d value of the density.
*/

void pd_pos_gne(double y, double t, double x,
	double *value_p, double *value_d)
{
	// probabilit� que le temps de sortie soit sup�rieur � t
	double alpha=1-p_exit_time(t,x);
	double v1,v2;
	idd_pos(y,t,x,&v1,&v2);
	*value_p=v1/alpha;
	*value_d=v2/alpha;
}

/**\brief Inverse function of the distribution function of the position
of the Brownian motion starting at
\e x and conditioned not to have left \f$[-1,1]\f$ before the time \e t. 

A bissection method is used. This function is
called by \p q_pos_gne() if the Newton method does not converge.

@param value Target value.
@param t Time.
@param x Initial position of the Brownian motion.
@param corr_error Instead of solving \f$F(u)/c=value\f$ for \f$ c>0\f$,
we compute \f$F^{-1}(c\times value)\f$.
The error is then multiplied by \p corr_error.
*/

double q_pos_gne_bissection(double value, double t,
	double x, double corr_error)
{
	double y_min=-0.99999;
	double y_max=0.99999;
	double y_middle;
	double error=1e-10;
	double error_eval=1e-10;
	double eval_max,eval_min;
	double eval_middle;
	error_eval*=corr_error;
	eval_min=id_pos(y_min,t,x)-value;
	eval_max=id_pos(y_max,t,x)-value;
	if (eval_min>0.0 || eval_max<0.0)
		return ERROR_q_pos_gne;
	do
		{
		y_middle=0.5*(y_max+y_min);
		eval_middle=id_pos(y_middle,t,x)-value;
		if (fabs(eval_middle)<error_eval)
			return y_middle;
		if (eval_middle<0.0)
			y_min=y_middle;
		else
			y_max=y_middle;
		}
	while (y_max-y_min>error);
	return y_middle;
}

/**\brief  This function aims to guess a first estimate
of the inverse of the distribution function \e F of 
the position of the Brownian motion at time \e t given not 
to have exit from \f$[-1,1]\f$.

The point which is 
returned is used to initialize the Newton algorithm
in \p q_pos_gne(). 

@param x Initial position of the Brownian motion.

The first term of the series giving the function \e F is equal to 
\f[F(t,x,y)=\frac{2}{\pi\mathbf{P}_x[t<\tau]}
\exp(-\pi^2t/8)\sin\left(\frac{\pi}{2}(x+1)\right)
\left(1-\cos\left(\frac{\pi}{2}(y+1)\right)\right),\f] 
where \f$\tau\f$ is the first exit time from \f$[-1,1]\f$.
We are willing to invert this function with respect to the variable \e y.
Indeed, this function returns \f$(2/\pi) \arccos(u)-1\f$
and for this, we use the table \p pos_gne_inverse_first_term(). 
For efficiency, the table gives an approximation with 2 digits.

@see pos_gne_inverse_first_term.
*/

double q_pos_gne_guess(double x)
{
	int index;
	index=(int) (100*x+100);
	if (index<0 || index>200)
		index=100;
	return pos_gne_inverse_first_term[index];
}

/**@brief Inverse function of the distribution function of the position
of the Brownian motion starting at
\e x and conditioned not to have left \f$[-1,1]\f$ before the time \e t. 

The Newton method is used. This function call 
\p q_pos_gne_bissection() when it fails to converge.

@param value Target value.
@param t Time.
@param x Initial position of the Brownian motion.
*/

double q_pos_gne(double value, double t,double x)
{
	double y_init=0.0;
	double y_next;
	double error=1e-10;
	double y_min=-1.0;
	double y_max=1.0;
	double eval_p,eval_d;
	int iter=0;
	int max_iter=100;
	int status=1;
	double oneminusexittime;

	// probabilit� que le temps de sortie soit sup�rieur � t
	oneminusexittime=1.0-p_exit_time(t,x);
	value=value*oneminusexittime;
	error=error*oneminusexittime;
	// estimation initiale de la position 
	// permet un gain d'environ 20%
	y_init=q_pos_gne_guess(1.0-value*M_PI_2*EXP(M_PI_SQUARE_OVER_8*t)/SIN((x+1)*M_PI_2));
	while (status)
	{
		iter++;
		idd_pos(y_init,t,x,&eval_p,&eval_d);
		eval_p-=value;
		if (fabs(eval_p)<error) 
			break;
		y_next=y_init-eval_p/eval_d;
		status=(y_next>y_min && y_next<y_max) && (iter<max_iter);
		y_init=y_next;
		}
		if (status) 
			return y_init;
		else
			return q_pos_gne_bissection
				(value,t,x,oneminusexittime);
}

/*@}*/

/**********************************************************************
SIMULATION
**********************************************************************/


/*****************************************************************
NOTE SUR LE GENERATEUR ALEATOIRE
*****************************************************************/

/* Le g�n�rateur al�atoire est celui de la GSL
Il est d�fini de la fa�on suivante 

	gsl_rng * rng;
	const gsl_rng_type * type_rng; 

et est initialis� par 

	gsl_rng_env_setup();
	type_rng=gsl_rng_default;
	rng=gsl_rng_alloc(type_rng);
*/

/*****************************************************************
VARIABLES ALEATOIRES DU MVT BROWNIEN TUE
*****************************************************************/

/** @addtogroup exittime */

/*@{*/

/**\brief  Return a realization of a random variable 
with the distribution of the first exit time
from \f$[-1,1]\f$ for the Brownian motion. 

@param *rng random number generator.
@param x initial position of the Brownian motion.
*/
double r_exit_time(gsl_rng *rng, double x)
{
	double value;
	double result;
	do 
	{
		value=SHORTEN(gsl_rng_uniform(rng));
		result=q_exit_time(value,x,1.0);
	}
	while (result==0.0); // 0.0 est la valeur d'erreur
	return result;
}


/**\brief  Return a realization of a random variable
whose distribution is that of the exit time from \f$[-1,1]\f$
of the Brownian motion starting at \p x and 
given it has exit \f$[-1,1]\f$ before time \p t.

@param *rng Random number generator.
@param t Time before which the Brownian motion has left \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.
*/

double r_exit_time_gets(gsl_rng *rng, double t,double x)
{
	double value;
	double result;
	double corr;
	unsigned int cpt=0;
	// if the value is too small
	if((corr=p_exit_time(t,x))<1e-10)
	    {return(t*gsl_rng_uniform(rng));}
	do 
	{
	    cpt++;
	    value=SHORTEN(gsl_rng_uniform(rng));
	    result=q_exit_time(corr*value,x,corr);
	}
	// un resultat de 0.0 est une erreur
	while ((result==0.0) && (cpt<20));
	if (result!=20)
	    {return result;}
	else
	    {return(t*gsl_rng_uniform(rng));}
}

/**\brief 
Return a realization of a random variable 
with the distribution of the first exit time
from \f$[-1,1]\f$ for the Brownian motion
that leaves \f$[-1,1]\f$ by the point 1.

@param *rng random number generator.
@param x Initial position of the Brownian motion.
*/

double r_exit_time_right(gsl_rng *rng, double x)
{
	double value;
	double result;
	do 
	{
		value=SHORTEN(gsl_rng_uniform(rng));
		result=q_exit_time_right(value,x,1.0);
	}
	while (result==0.0); // 0.0 est la valeur d'erreur
	return result;
}


/**
\brief Return a realization of a random variable 
with the distribution of the first exit time
from \f$[-1,1]\f$ for the Brownian motion
that leaves \f$[-1,1]\f$ by the point 1
given the exit time is smaller than \e t.

@param *rng random number generator.
@param t Time before which the Brownian motion has left \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.

@retval Return a positive real (double).

@warning The loop may be infinite.
*/

double r_exit_time_right_gets(gsl_rng *rng, double t, double x)
{
	double value;
	double result;
	double corr;
	double random;
	corr=p_exit_time_right(t,x);
	do 
	{
		random=SHORTEN(gsl_rng_uniform(rng));
		value=corr*random;
		result=q_exit_time_right(value,x,corr);
	}
	while (result==ERROR_q_exit_time);
	return result;
}

/**
\brief Return the exit time given it happens at the right
end point given its exit time is smaller than \e t
with a counter in case of failure.
See \e r_exit_time_right_gets for more information.

@param *rng random number generator.
@param t Time before which the Brownian motion has left \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.

@retval Return a positive real (double).
*/
double r_exit_time_right_gets_security(gsl_rng *rng, double t, double x)
{
	double value;
	double result;
	double corr;
	double random;
	unsigned int cpt=0;
	corr=p_exit_time_right(t,x);
	do 
	{
		cpt++;
		random=SHORTEN(gsl_rng_uniform(rng));
		value=corr*random;
		result=q_exit_time_right(value,x,corr);
	}
	while (result==ERROR_q_exit_time && cpt<100);
	return result;
}

/*@}*/

/** @addtogroup posBM */

/*@{*/

/**\brief Return a realization of a random variable whose distribution
is that of the position of the Brownian motion at time \p t
given it has not left \f$[-1,1]\f$ before \p t.

@retval Return a value (double) in \f$[-1,1]\f$.
The error \p ERROR_r_pos_gne value may be returned
in case of failure.

@param *rng random number generator.
@param t Time after which the Brownian motion may leave \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.
*/
double r_pos_gne(gsl_rng *rng, double t,double x)
{
        double result;
	double value;
        do 
        {
		// on change l'�chelle pour �viter les probl�mes num�riques
                value=SHORTEN(gsl_rng_uniform(rng));
		result=q_pos_gne(value,t,x);
        }
        while(result==ERROR_r_pos_gne); 
        return result;
}



/**\brief Return a realization of a random variable whose distribution
is that of the position of the Brownian motion at time \p t
given it has not left \f$[-1,1]\f$ before \p t.

The difference with \p r_pos_gne() is that the 
number of iterations in case of errors in bounded.

@retval Return a value (double) in \f$[-1,1]\f$.
The error \p ERROR_r_pos_gne value may be returned
in case of failure.

@param *rng random number generator.
@param t Time after which the Brownian motion may leave \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.
@param *status Is set to 1 or 0 according to a success of a failure.
*/
double r_pos_gne_check(gsl_rng *rng, double t,double x,int *status)
{
        double result;
	double value;
	int cpt=0;
        do 
        {
		// on change l'�chelle pour �viter les probl�mes num�riques
                value=SHORTEN(gsl_rng_uniform(rng));
		result=q_pos_gne(value,t,x);
		cpt++;
        }
        while(result==ERROR_r_pos_gne && cpt<MAX_ITER_r_pos_gne); 
	*status=(cpt<MAX_ITER_r_pos_gne);
        return result;
}

/*****************************************************************
POSITION DU BROWNIEN DANS \f$[-1,1]\f$ LORSQUE LE TEMPS DE SORTIE EST 
PLUS GRAND QU'UNE VALEUR FIXEE
*****************************************************************/

/**@brief Return a realization of a random variable whose
distribution is the position of the Brownian motion 
at time \p t given that its exit time from \f$[-1,1]\f$
is greater than \p theta.

\pre The argument \p theta shall be greater than \p t (not checked). 

@param *rng random number generator.
@param t Time.
@param theta Time after which the Brownian motion may leave \f$[-1,1]\f$.
@param x Initial position of the Brownian motion.
*/

double r_pos_getg(gsl_rng *rng, double t, double theta, double x)
{
	double value;
	double unif;
	int cpt;
	if (theta==t)
		return(r_pos_gne(rng,t,x));
	cpt=0;
	do 
	{
		value=r_pos_gne(rng,t,x);
		unif=gsl_rng_uniform(rng);	
		cpt++;
	}
	while ((p_exit_time(theta-t,value)>=unif) && (cpt<40));
	if (cpt==40) 
		return ERROR_r_pos_gne;
	return (value);
}

/*****************************************************************
POSITION DU BROWNIEN DANS [-1,1] LORSQUE LE TEMPS DE SORTIE EST FIX�
*****************************************************************/

/** \brief 
Return an upper bound of the density of the exit 
time from \f$[-1,1]\f$ for the Brownian motion given 
it exits from \f$[-1,1]\f$ at point 1.

The bound is uniform with respect to the starting point.

This function is called by \p r_pos_right_gete().
*/

double bound_d_exit_time_right(double time)
{
	int N=10; // nombre de termes de la serie
	int n;
	int n_square;
	double bound;
	double tmp;
	int signe=1;
	// Pour les petits temps, on utilise une table...
	// Les valeurs sont obtenues empiriquement
	if (time<1.0)
		{
		if (time>0.6)
			return 0.8;	
		else if (time>0.5)
			return 0.9;
		else if (time>0.4)
			return 1.0;
		else if (time>0.3)
			return 1.2;
		else if (time>0.2)
			return 1.6;
		else if (time>0.1)
			return 3.0;
		else if (time>0.05)
			return 6.0;
		else if (time>0.01)
			return 27.0;
		else 
			return 250.0; // Ici, il faut time>0.001
		}
	else
		{
		tmp=M_PI_SQUARE_OVER_8*time;
		bound=0.0;
		for(n=1;n<=N;n++)
			{
			n_square=n*n;
			bound+=signe*n_square*EXP(-n_square*tmp);	
			signe=-signe;
			}
		bound=bound*M_PI_2_SQUARE;
		return bound;
		}
}

/**\brief Position of the Brownian motion at time \p time
when it exits from \f$[-1,1]\f$ at time \p finaltime
and by point 1.

@remark This function relies on a rejection method and is slow.

@param *rng random number generator.
@param *time Time.
@param finaltime Time at which the exit time from \f$[-1,1]\f$ holds.
@param x Initial position of the Brownian motion.

@retval Return a value (double) in \f$[-1,1]\f$ or \p ERROR_r_pos_gne.
*/

double r_pos_right_gete(gsl_rng *rng, double time, double finaltime, double x)
{
	double remainingtime;
	double phi,psi;
	double maxphi;
	double rv_U;
	double rv_X;
	int compteur;

	remainingtime=finaltime-time;
#ifdef ASSERT_MODE
	assert(finaltime-time>0);
#endif
	psi=(1-p_exit_time(time,x))/d_exit_time(finaltime,x)/(1+x);
	// version sans conditionnement
	maxphi=	2*psi*bound_d_exit_time_right(remainingtime);
	if (maxphi<1.0)
		 maxphi=1.0;
	psi=psi/maxphi;
	compteur=0;
	while (compteur<MAX_ITER_r_pos_right_gete) // il se peut que ca boucle...
	{
		compteur++;
		rv_U=SHORTEN(gsl_rng_uniform(rng));
		rv_X=r_pos_gne(rng,time,x); // on utilise la position du brownien sans conditionnement
		phi=psi*d_exit_time_right(remainingtime,rv_X)*(1+rv_X);
		if (rv_U<=phi)
			return rv_X;
	}
	return ERROR_r_pos_gne; // In case of failure.
}

/*@}*/

/**********************************************************************
FIN DU FICHIER
**********************************************************************/
