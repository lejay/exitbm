/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
#ifndef EXITBM_H
#define EXITBM_H
/** @file exitbm.h 

@brief Functions related to the exit time
and the position of a Brownian motion in an interval.

@author Antoine Lejay
@version 2.3
@date April 20, 2012

Computes the densities and distribution functions
related to the exit time and position from an interval
for the Brownian motion. Allows us to simulate some random
variables with these distributions.

For random variates simulation, the variable 
\e rng shall be defined the following way
(by using the GSL random number generation):
@code
    gsl_rng * rng;
    const gsl_rng_type * type_rng; 
@endcode
and is initialize by 
@code
    gsl_rng_env_setup();
    type_rng=gsl_rng_default;
    rng=gsl_rng_alloc(type_rng);
@endcode
See the GSL manual.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_exp.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_trig.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/** @defgroup errors Error values */
/*@{*/
/** \brief Error value for q_exit_time */
#define ERROR_q_exit_time 0.0
/** \brief Error value for q_pos_gne */
#define ERROR_q_pos_gne 2.0
/** \brief Error value for r_pos_gne */
#define ERROR_r_pos_gne 2.0
/*@}*/

/** @defgroup debug Debugging facilities */
/*@{*/
/** In this mode, the \p asssert functions are evaluated for testing parameters. 
 */
#define ASSERT_MODE
/*@}*/

/** @defgroup mathconst Mathematical constants */
/*@{*/
/** Set the value to \f$\pi^2/2\f$. */
const double M_PI_SQUARE_OVER_2=M_PI*M_PI/2.0; 
/** Set the value to \f$\pi^2/8\f$. */
const double M_PI_SQUARE_OVER_8=M_PI*M_PI/8.0; 
/** Set the value to \f$\pi^2/4\f$. */
const double M_PI_2_SQUARE=M_PI_2*M_PI_2;
/** Set the value to \f$4/\pi\f$. */
const double M_4_OVER_PI=4/M_PI;
/** Set the value to \f$\pi/8\f$. */
const double M_PI_8=M_PI/8.0;
/**@brief Shorten the uniform range to avoid problems. */
#define SHORTEN(x) (1e-5+0.99998*x)
/*@}*/

/** @defgroup GSLwrapper Wrapper for the GSL */
/*@{*/
/**\brief Redefinition of the exponential to avoid overflow/underflow errors */
#define EXP(x)  secure_exp(x) 
/**\brief  Wrapper to the GSL logarithm */
#define LOG(x) gsl_sf_log(x)
/**\brief  Wrapper to the GSL erf function */
#define ERF(x) gsl_sf_erf(x)
/**\brief  Wrapper to the GSL erfc function */
#define ERFC(x) gsl_sf_erfc(x)
/**\brief  Wrapper to the GSL sine function */
#define SIN(x) gsl_sf_sin(x)
/**\brief  Wrapper to the GSL cosine function */
#define COS(x) gsl_sf_cos(x)
/**\brief  Wrapper to the GSL square function */
#define POW2(x) gsl_pow_2(x)
/*@}*/

double secure_exp(double x);

/* EXIT TIME */

// distribution function 
double p_exit_time(double t, double x);

// density
double d_exit_time(double t, double x);

// distribution function and density (quicker)
void  pd_exit_time(double t, double x,double *value_p, double *value_d);

// inverse of the distribution function
double q_exit_time(double value, double x, double corr_error);

// random variable 
double r_exit_time(gsl_rng *rng, double x);

// random variable conditioned to be smaller than t
double r_exit_time_gets(gsl_rng *rng, double t,double x);

/* EXIT TIME WITH EXIT ON THE RIGHT */

// density
double d_exit_time_right(double t,double x);

// distribution function 
double p_exit_time_right(double t,double x);

// distribution function and density (quicker)
void pd_exit_time_right(double t, double x, double *value_p, double *value_d);

// inverse of the distribution function
double q_exit_time_right(double value, double x, double corr_error);

// random variable
double r_exit_time_right(gsl_rng *rng, double x);

// random variable conditioned to be smaller than t
double r_exit_time_right_gets(gsl_rng *rng, double t, double x);

double r_exit_time_right_gets_security(gsl_rng *rng, double t, double x);

/* DENSITY OF THE KILLED BROWNIAN MOTION */

// density (fundamental solution, not the density of a random variable)
double d_pos(double u, double t, double x);

// integral of the density (this is not a distribution function)
double id_pos(double u, double t, double x);

// integral of the density and density (quicker)
void idd_pos(double y, double t, double x, double *value_p, double *value_d);

/* DENSITY OF THE BROWNIAN MOTION CONDITIONED TO HAVE NOT EXIT FROM (-1,1) */

// density
double d_pos_gne(double y, double t, double x);

// distribution function 
double p_pos_gne(double y, double t, double x);

// distribution function and density (quicker)
void pd_pos_gne(double y, double t, double x, double *value_p, double *value_d);

// inverse of the distribution function
double q_pos_gne(double value, double t,double x);

// random variable
double r_pos_gne(gsl_rng *rng, double t,double x);

// random variable with a check that it is correct
double r_pos_gne_check(gsl_rng *rng, double t,double x,int *status);

// random variable given the exit time is greater than theta
double r_pos_getg(gsl_rng *rng, double t, double theta, double x);

// random variable given the exit time is equal to finaltime
double r_pos_right_gete(gsl_rng *rng, double time, double finaltime, double x);

#endif
/**********************************************************************
END OF FILE
**********************************************************************/

