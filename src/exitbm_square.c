/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
/** @file exitbm_square.c 

@brief Functions related to the exit time
and the position of a Brownian motion in a square,
when the initial position of the Brownian motion
is the center of the square.

The computations are taken from the article
Milstein, G. N.; Tretyakov, M. V.;
Simulation of a space-time bounded diffusion. <EM>Ann. Appl. Probab.</EM> 9 (1999), no. 3, 732--779. 

Uses the libraries @link exitbm.c exitbm@endlink and
@link exitbm_fz.c exitbm_fz@endlink.

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#include <exitbm_square.h>


/** \addtogroup mathfunctions */
/**@{*/
/** \brief Returns \f$x^{y}\f$ */
double floatingpower(double x,double y)
{
    return(gsl_sf_exp(y*gsl_sf_log(x)));
}
/**@}*/

/** \defgroup ExitTimePositionSquare Exit time time and position from a square. 
 *
 * @brief Simulate the first exit time and position from a square for a Brownian particle.
 */

/** \addtogroup ExitTimePositionSquare */
/*@{*/

/** \brief Distribution function of the exit time from a square. */
double p_exit_time_square(double t)
{
	double tmp;
	tmp=1.0-p_exit_time_fz(t);
	return 1.0-tmp*tmp;
}

/** \brief Distribution function of the exit time from an hypercube. */
double p_exit_time_hypercube(double t, unsigned int dim)
{
	double tmp;
	tmp=1.0-p_exit_time_fz(t);
	return 1.0-gsl_pow_int(tmp,dim);
}


/** \brief Returns a realization of the exit time from an hypercube.
 *
 * \param[in] rng Random number generator.
 * \param[in] dim Dimension of the space.
 *
 * \return A realization of the exit time from an hypercube for a Brownian motion.
 * Returns the error code \a ERROR_q_exit_time after 20 successive failures.
 * */

double r_exit_time_hypercube(gsl_rng *rng, unsigned int dim)
{
	double value;
	double result;
	int cpt=0;
	do 
	{
		value=(1.0-pow(gsl_rng_uniform(rng),1.0/dim))*0.9998+0.0001;
		result=q_exit_time_fz(value);
		cpt++;	
	}
	while (result==ERROR_q_exit_time || cpt==20); 
	return (cpt==20 ? ERROR_r_exit_time_square : result);
}

/** \brief Returns a realization of the exit time from a square.
 *
 * \param[in] rng Random number generator.
 *
 * \return A realization of the exit time from an square for a Brownian motion.
 * Returns the error code \a ERROR_q_exit_time after 20 successive failures.
 * */

double r_exit_time_square(gsl_rng *rng)
{
	double value;
	double result;
	int cpt=0;
	do 
	{
		value=(1.0-sqrt(gsl_rng_uniform(rng)))*0.9998+0.0001;
		result=q_exit_time_fz(value);
		cpt++;	
	}
	while (result==ERROR_q_exit_time || cpt==20); 
	return (cpt==20 ? ERROR_r_exit_time_square : result);
}

/**********************************************************************
EXIT TIME FROM A SPACE-TIME RIGHT PARALLELEPIPED WITH CUBIC BASE
**********************************************************************/
/** \brief Returns a realization of a random variable
with the exit time from a square for the Brownian motion
started from its center. A correction term 
may be added to get a correction \f$c\f$ for simulating random variables
with some conditions.

This value is from the exit time from an interval
by the formula 
\f[
\tau=F^{-1}(1-(1-\mathrm{unif}*c)^{1/2}).
\f]

\param[in] rng Random number generator.
\param[in] correction The correction term.

\return A realization of the exit time from an square for a Brownian motion.
Returns the error code \a ERROR_q_exit_time after 20 successive failures.
*/
double r_exit_time_square_correction(gsl_rng *rng, double correction)
{
	double value;
	double result;
	int cpt=0;
	do 
	{
		value=(1.0-sqrt(1.0-gsl_rng_uniform(rng)*correction))*0.9998+0.0001;
		result=q_exit_time_fz(value);
		cpt++;	
	}
	while (result==0.0 || cpt==20); // 0.0 is a value to be rejected.
	return (cpt==20 ? ERROR_r_exit_time_square : result);
}

/** \brief Returns a realization of a random variable
with the exit time from a square for the Brownian motion
started from its center, conditionned to be smaller
than a given value of \f$t\f$.

This value is deduced from the exit time from an interval
by the formula 
\f[
\tau=F^{-1}(1-(1-\mathrm{unif}*(1-(1-F(t))^2))^{1/2}).
\f]

\param[in] rng Random number generator.
\param[in] time The time \f$t\f$.

\return A realization of the exit time from an square for a Brownian motion conditioned to be smaller than \f$t\f$.
Returns the error code \a ERROR_q_exit_time after 20 successive failures.
*/

double r_exit_time_square_gets(gsl_rng *rng, double time)
{
	double value;
	double result;
	double tmp;
	int cpt=0;
	do 
	{
		tmp=1.0-p_exit_time_fz(time);
		value=(1.0-sqrt(1.0-gsl_rng_uniform(rng)*(1-tmp*tmp)))*0.9998+0.0001;
		result=q_exit_time_fz(value);
		cpt++;	
	}
	while (result==0.0 || cpt==20); // 0.0 est la valeur d'erreur
	return (cpt==20 ? ERROR_r_exit_time_square : result);
}

/** \brief Distribution function of the exit time from an hypercube. */

/** Returns a realization of a random variable
with the exit time from an hypercube for the Brownian motion
started from its center, conditionned to be smaller
than a given value of \f$t\f$.

This value is deduced from the exit time from an interval
by the formula 
\f[
\tau=F^{-1}(1-(1-\mathrm{unif}*(1-(1-F(t))^d))^{1/d}).
\f]

\param[in] rng Random number generator.
\param[in] time The time \f$t\f$.
\param[in] dim The dimension of the hypercube.

\return A realization of the exit time from an square for a Brownian motion conditioned to be smaller than \f$t\f$.
Returns the error code \a ERROR_q_exit_time after 20 successive failures.
**/
double r_exit_time_hypercube_gets(gsl_rng *rng, double time, unsigned int dim)
{
	double value;
	double result;
	double tmp;
	int cpt=0;
	do 
	{
		tmp=1.0-p_exit_time_fz(time);
		tmp=(1.0-gsl_rng_uniform(rng)*(1.0-gsl_pow_int(tmp,dim)));
		value=(1.0-floatingpower(tmp,1.0/dim))*0.9998+0.0001;
		result=q_exit_time_fz(value);
		cpt++;	
	}
	while (result==0.0 || cpt==20); // 0.0 is a value to be rejected.
	return (cpt==20 ? ERROR_r_exit_time_square : result);
}



/**********************************************************************
EXIT TIME AND POSITION FROM A SPACE-TIME RIGHT PARALLELEPIPED WITH CUBIC BASE
**********************************************************************/


/**
\brief Returns a realization of 
\f$(t\wedge\tau,B_{t\wedge\tau})\f$, where 
\f$\tau\f$ is the first exit time 
from the square \f$[-1,1]^2\f$ with \f$B_0=0\f$.

\param[in] rng Random number generator.
\param[in] final_time The exit time  \f$f\f$.
\param[out] exit_time Value of \f$\tau\wedge t\f$.
\param[out] pos_x \f$x\f$-coordinate of the position of \f$B_{\tau\wedge t}\f$.
\param[out] pos_y \f$y\f$-coordinate of the position of \f$B_{\tau\wedge t}\f$.
*/

int r_exit_time_position_space_time_square(gsl_rng *rng, 
	double final_time, double *exit_time,
	double *pos_x, double *pos_y)
{
	double alpha;

	alpha=1.0-POW2(1.0-p_exit_time_fz(final_time));
	if (gsl_rng_uniform(rng)<alpha)
		{
		// In this case, \tau<T 
		*exit_time=r_exit_time_square_correction(rng,alpha);
		if (*exit_time==ERROR_r_exit_time_square)
			return 0; 
		if (gsl_rng_uniform(rng)>0.5)
			{
			*pos_x=r_pos_gne_fz(rng,*exit_time);
			*pos_y=(gsl_rng_uniform(rng)>0.5 ? 1 : -1);
			}
		else
			{
			*pos_y=r_pos_gne_fz(rng,*exit_time);
			*pos_x=(gsl_rng_uniform(rng)>0.5 ? 1 :-1);
			}
		}
	else
		{
		*exit_time=final_time;
		if (final_time>30.0)
			return 0;
		*pos_x=r_pos_gne_fz(rng,final_time);
		*pos_y=r_pos_gne_fz(rng,final_time);
		}
	return 1;
}


/**********************************************************************
EXIT TIME AND POSITION FROM A SQUARE
**********************************************************************/

/** \brief Returns a realization of 
\f$(tau,B_{\tau})\f$, where 
\f$\tau\f$ is the first exit time 
from the square \f$[-1,1]^2\f$ with \f$B_0=0\f$.

\param[in] rng Random number generator.
\param[out] exit_time Contains the exit time.
\param[out] pos_x \f$x\f$-coordinate of the position of \f$B_{\tau}\f$.
\param[out] pos_y \f$y\f$-coordinate of the position of \f$B_{\tau}\f$.
*/


int r_exit_time_position_square(gsl_rng *rng, 
	double *exit_time,
	double *pos_x, double *pos_y)
{
	*exit_time=r_exit_time_square(rng);
	if (*exit_time==ERROR_r_exit_time_square)
		return 0; 
	if (gsl_rng_uniform(rng)>0.5)
		{
		*pos_x=r_pos_gne_fz(rng,*exit_time);
		*pos_y=(gsl_rng_uniform(rng)>0.5 ? 1 : -1);
		}
	else
		{
		*pos_y=r_pos_gne_fz(rng,*exit_time);
		*pos_x=(gsl_rng_uniform(rng)>0.5 ? 1 :-1);
		}
	return 1;
}

/*@}*/

/**********************************************************************
END OF TILE.
**********************************************************************/



