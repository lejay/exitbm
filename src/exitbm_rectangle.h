/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
#ifndef EXITBM_RECTANGLE_H
#define EXITBM_RECTANGLE_H
/** @file exitbm_rectangle.h 

@brief Functions for simulating the first exit time and position 
from a rectangle.

Simulate the first exit time and position
from a rectangle, or from a time-space
parallelepiped, which means the first 
exit time and position if it happens before
a fixed time, or the position at this time
if the particle has not left the rectangle
at this time.

Uses the library @link exitbm.c exitbm@endlink,
@link exitbm_fz.c exitbm_fz@endlink and
@link exitbm_square.c exitbm_square@endlink.

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#include <exitbm.h>
#include <exitbm_square.h>
#include <exitbm_fz.h>

int r_exit_time_position_rectangle(gsl_rng *rng,
	double half_width, double half_height,
	double SQ_half_width, double SQ_half_height,
	double xpos, double ypos,
	double *exit_time, double *exit_x_position, double *exit_y_position,
	int *side);

int r_exit_time_position_rectangle_rws(gsl_rng *rng,
	double half_width, double half_height,
	double xpos, double ypos,
	double *exit_time, double *exit_x_position, double *exit_y_position,
	int *side);

int r_exit_time_position_space_time_rectangle(gsl_rng *rng,
	double half_width, double half_height,
	double SQ_half_width, double SQ_half_height,
	double finaltime,
	double xpos, double ypos,
	double *exit_time, double *exit_x_position, double *exit_y_position,
	int *side);

int r_exit_time_position_space_time_rectangle_rws(gsl_rng *rng,
	double half_width, double half_height, double final_time,
	double xpos, double ypos,
	double *exit_time, double *x, double *y,
	int *side);

#endif
