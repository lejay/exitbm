/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
#ifndef EXITBM_SQUARE_H
#define EXITBM_SQUARE_H
/** @file exitbm_square.h 

@brief Functions related to the exit time
and the position of a Brownian motion in a square,
when the initial position of the Brownian motion
is the center of the square.

Uses the libraries @link exitbm.c exitbm@endlink and
@link exitbm_fz.c exitbm_fz@endlink.

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#include <exitbm.h>
#include <exitbm_fz.h>
#include <gsl/gsl_sf_log.h>

/**\brief Error value for \p r_exit_time_square. */
#define ERROR_r_exit_time_square -1.0

double p_exit_time_square(double t);

double p_exit_time_hypercube(double t, unsigned int dim);

double r_exit_time_square(gsl_rng *rng);

double r_exit_time_hypercube(gsl_rng *rng, unsigned int dim);

double r_exit_time_hypercube_gets(gsl_rng *rng, double time, unsigned int dim);

double r_exit_time_square_correction(gsl_rng *rng, double correction);

double r_exit_time_square_gets(gsl_rng *rng, double time);

double r_pos_gne_fz(gsl_rng *rng, double time);

int r_exit_time_position_space_time_square(gsl_rng *rng, 
	double final_time, double *exit_time,
	double *pos_x, double *pos_y);

int r_exit_time_position_square(gsl_rng *rng, 
	double *exit_time,
	double *pos_x, double *pos_y);

#endif
