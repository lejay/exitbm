/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011,2012
**********************************************************************/
/** \file exitbm_benchmark.c

\brief Code to benchmark the library \a exitbm.

Mostly call the functions (random variable generation) a large
number of times with random parameters.

The names are of type \a benchmark_function, where \a function is the name of a function (for compatibility with
the code generator \a test_benchmark.rb in ruby).

The argument \a nb of each function is the number of samples. 

Uses \c exitbm
\c exitbm_fz.c
\c exitbm_square.c
\c exitbm_fhor.c
and 
\c exitbm_rectangle.c

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/

#include "exitbm_benchmark.h"


/**\defgroup Benchmark Functions for benchmarking*/
/**\addtogroup Benchmark */
/*@{*/

/**\def INIT_GENERATOR
 * \brief Definition of random number generator called rng.
 */
#define INIT_GENERATOR gsl_rng * rng; const gsl_rng_type * type_rng; gsl_rng_env_setup(); type_rng=gsl_rng_default; rng=gsl_rng_alloc(type_rng);

/** \brief Benchmark for \a r_exit_time.
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time(unsigned int nb)
{
    INIT_GENERATOR

    unsigned int i;
    double x;

    for(i=0;i<nb;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_exit_time(rng,x);
	    }
}

/** \brief Benchmark for \a r_exit_time_fz.
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_fz(unsigned int nb)
{
    INIT_GENERATOR
    unsigned int i;

    for(i=0;i<nb;i++)
	    {
	    r_exit_time_fz(rng);
	    }
}

/** \brief Benchmark for \a r_exit_time_gets.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_gets(unsigned long int nb)
{
    INIT_GENERATOR
    double t,x;
    unsigned long int i;

    for(i=0;i<nb;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_gets(rng,t,x);
	    }
}

/** \brief Benchmark for \a r_exit_time_gets_fz.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_gets_fz(unsigned int nb)
{
    INIT_GENERATOR
    unsigned int i;
    double t;
	
    for(i=0;i<nb;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_gets_fz(rng,t);
	    }
}

/** \brief Benchmark for \a r_exit_time_right.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_right(unsigned int nb)
{
    INIT_GENERATOR
    double x;
    unsigned int i;
	
    for(i=0;i<nb;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_exit_time_right(rng,x);
	    }
}

/** \brief Benchmark for \a r_exit_time_right_gets.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_right_gets(unsigned int nb)
{
    INIT_GENERATOR
    double t,x;
    unsigned int i;

    for(i=0;i<nb;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    t=gsl_ran_exponential(rng,1.0);
	    r_exit_time_right_gets(rng,t,x);
	    }
}

/** \brief Benchmark for \a r_pos_gne.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_pos_gne(unsigned int nb)
{
    INIT_GENERATOR
    double t,x;
    unsigned int i;
	

    for(i=0;i<nb;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_gne(rng,t,x);
	    }
}

/** \brief Benchmark for \a r_pos_gne_fz.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_pos_gne_fz(unsigned int nb)
{
    INIT_GENERATOR
    double t;
    unsigned int i;
	
    for(i=0;i<nb;i++)
	    {
	    t=gsl_ran_exponential(rng,1.0);
	    r_pos_gne_fz(rng,t);
	    }
}

/** \brief Benchmark for \a r_pos_getg.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_pos_getg(unsigned int nb)
{
    INIT_GENERATOR
    double t,x,theta;
    unsigned int i;

    for(i=0;i<nb;i++)
	    {
	    theta=gsl_ran_exponential(rng,1.0);
	    t=gsl_rng_uniform(rng)*theta;
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_getg(rng,t,theta,x);
	    }
}

/** \brief Benchmark for \a r_pos_gete.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_pos_gete(unsigned int nb)
{   
    INIT_GENERATOR
    double t,x,theta;
    unsigned int i;

    for(i=0;i<nb;i++)
	    {
	    theta=gsl_ran_exponential(rng,1.0);
	    t=gsl_rng_uniform(rng)*theta;
	    x=2*gsl_rng_uniform(rng)-1.0;
	    r_pos_right_gete(rng,t,theta,x);
	    }
}

/** \brief Benchmark for \a r_exit_time_position_space_time_square.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_position_space_time_square(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time,final_time;
    unsigned int i;

    for(i=0;i<nb;i++)
    {
	final_time=gsl_ran_exponential(rng,1.0);
	r_exit_time_position_space_time_square(rng,final_time,&time,&x,&y);
    }
}

/** \brief Benchmark for \a r_exit_time_position_square.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_position_square(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time;
    unsigned int i;

    for(i=0;i<nb;i++)
    {
	r_exit_time_position_square(rng,&time,&x,&y);
    }
}

/** \brief Benchmark for \a r_exit_time_position_rectangle.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_position_rectangle(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time;
    double sq_half_width;
    double sq_half_height;
    int side;
    double half_width, half_height, init_x, init_y;
    unsigned int i;

    for(i=0;i<nb;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	sq_half_width=half_width*half_width;
	sq_half_height=half_height*half_height;
	r_exit_time_position_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,init_x,init_y,&time,&x,&y,&side);
    }
}

/** \brief Benchmark for \a r_exit_time_position_rectangle_rws.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_position_rectangle_rws(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time;
    int side;
    double half_width, half_height, init_x, init_y;
    unsigned int i;

    for(i=0;i<nb;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	r_exit_time_position_rectangle_rws(rng,half_width,half_height,init_x,init_y,&time,&x,&y,&side);
    }
}

/** \brief Benchmark for \a r_exit_time_position_space_time_rectangle_rws.
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_position_space_time_rectangle(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time;
    double sq_half_width;
    double sq_half_height;
    int side;
    double half_width, half_height, init_x, init_y;
    double final_time;

    int i;

    for(i=0;i<nb;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	final_time=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	sq_half_width=half_width*half_width;
	sq_half_height=half_height*half_height;
	r_exit_time_position_space_time_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,final_time,init_x,init_y,&time,&x,&y,&side);
    }
}

/** \brief Benchmark for \a r_exit_time_position_space_time_rectangle_rws.
 *
 * \param[in] nb Number of samples.
 */
void benchmark_r_exit_time_position_space_time_rectangle_rws(unsigned int nb)
{
    INIT_GENERATOR
    double x,y,time;
    double sq_half_width;
    double sq_half_height;
    int side;
    double half_width, half_height, init_x, init_y;
    double final_time;

    int i;

    for(i=0;i<nb;i++)
    {
	half_width=gsl_ran_exponential(rng,1.0);
	half_height=gsl_ran_exponential(rng,1.0);
	final_time=gsl_ran_exponential(rng,1.0);
	init_x=(2*gsl_rng_uniform(rng)-1)*half_width;
	init_y=(2*gsl_rng_uniform(rng)-1)*half_height;
	sq_half_width=half_width*half_width;
	sq_half_height=half_height*half_height;
	r_exit_time_position_space_time_rectangle_rws(rng,half_width,half_height,final_time,init_x,init_y,&time,&x,&y,&side);
    }
}

/** \brief Benchmark for \a r_exit_time_position_before_horizon
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_position_before_horizon(unsigned int nb)
{
    INIT_GENERATOR

    unsigned int i;
    double x,horizon;
    double time,position;
    int exit;

    for(i=0;i<nb;i++)
	    {
	    x=2*gsl_rng_uniform(rng)-1.0;
	    horizon=gsl_ran_exponential(rng,1.0);
	    r_exit_time_position_before_horizon(rng,horizon, x, &time, &position, &exit);
	    }
}

/** \brief Benchmark for \a r_exit_time_position_before_horizon_fz without using \p exitbm_fz
 *
 * To be compared with \p benchmark_r_exit_time_position_before_horizon_fz
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_position_before_horizon_fz_without(unsigned int nb)
{
    INIT_GENERATOR

    unsigned int i;
    double horizon;
    double time,position;
    int exit;

    for(i=0;i<nb;i++)
	    {
	    horizon=gsl_ran_exponential(rng,1.0);
	    r_exit_time_position_before_horizon(rng,horizon, 0.0, &time, &position, &exit);
	    }
}

/** \brief Benchmark for \a r_exit_time_position_before_horizon_fz
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_position_before_horizon_fz(unsigned int nb)
{
    INIT_GENERATOR

    unsigned int i;
    double horizon;
    double time,position;
    int exit;

    for(i=0;i<nb;i++)
	    {
	    horizon=gsl_ran_exponential(rng,1.0);
	    r_exit_time_position_before_horizon_fz(rng,horizon, &time, &position, &exit);
	    }
}

/** \brief Benchmark for \a r_exit_time_position_before_horizon_sbm
 *
 * \param[in] nb Number of samples.
 */

void benchmark_r_exit_time_position_before_horizon_sbm(unsigned int nb)
{
    INIT_GENERATOR

    unsigned int i;
    double horizon,skew;
    double time,position;
    int exit;

    for(i=0;i<nb;i++)
	    {
	    horizon=gsl_ran_exponential(rng,1.0);
	    skew=gsl_rng_uniform(rng);
	    r_exit_time_position_before_horizon_sbm(rng,horizon,skew, &time, &position, &exit);
	    }
}

/*@}*/
