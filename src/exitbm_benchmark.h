/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
/** \file exitbm_benchmark.h

\brief Code to benchmark the library \a exitbm.

Mostly call the functions (random variable generation) a large
number of times with random parameters.

The names are of type \a benchmark_function, where \a function is the name of a function (for compatibility with
the code generator \a test_benchmark.rb in ruby).

The argument \a nb of each function is the number of samples. 

Uses \c exitbm
\c exitbm_fz.c
\c exitbm_square.c
\c exitbm_fhor.c
and 
\c exitbm_rectangle.c

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#ifndef EXITBM_BENCHMARK_H
#define EXITBM_BENCHMARK_H

#include "exitbm.h"
#include "exitbm_fz.h"
#include "exitbm_fhor.h"
#include "exitbm_square.h"
#include "exitbm_rectangle.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_log.h"
#include "gsl/gsl_sf_log.h"
#include "gsl/gsl_randist.h"

void benchmark_r_exit_time(unsigned int nb);

void benchmark_r_exit_time_fz(unsigned int nb);

void benchmark_r_exit_time_gets(unsigned long int nb);

void benchmark_r_exit_time_gets_fz(unsigned int nb);

void benchmark_r_exit_time_right(unsigned int nb);

void benchmark_r_exit_time_right_gets(unsigned int nb);

void benchmark_r_pos_gne(unsigned int nb);

void benchmark_r_pos_gne_fz(unsigned int nb);

void benchmark_r_pos_getg(unsigned int nb);

void benchmark_r_pos_gete(unsigned int nb);

void benchmark_r_exit_time_position_space_time_square(unsigned int nb);

void benchmark_r_exit_time_position_square(unsigned int nb);

void benchmark_r_exit_time_position_rectangle(unsigned int nb);

void benchmark_r_exit_time_position_rectangle_rws(unsigned int nb);

void benchmark_r_exit_time_position_space_time_rectangle(unsigned int nb);

void benchmark_r_exit_time_position_space_time_rectangle_rws(unsigned int nb);

void benchmark_r_exit_time_position_before_horizon(unsigned int nb);

void benchmark_r_exit_time_position_before_horizon_fz_without(unsigned int nb);

void benchmark_r_exit_time_position_before_horizon_fz(unsigned int nb);

#endif EXITBM_BENCHMARK_H
