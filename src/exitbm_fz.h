/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
#ifndef EXITBM_FZ_H
#define EXITBM_FZ_H
/** @file exitbm_fz.h 

@brief Optimized version of the exit time 
of a Brownian motion from the interval \f$[-1,1]\f$
starting from \f$0\f$.

Provides functions similar to the one
in \link exitbm.c exitbm\endlink but that 
are optimized (use of tabulated values, ...). 

Use the library @link exitbm.c exitbm@endlink.

@author Antoine Lejay
@version 2.3
@date April 20, 2012

**/

#include "gsl/gsl_complex_math.h"
#include "gsl/gsl_complex.h"
#include "exitbm.h"

/** \defgroup ExitTimeFromZero Exit time time and position from an interval for a Brownian motion starting at its center.
 *
 * @brief Fast procedures when the Brownian motion starts from zero.
 * */

/** \addtogroup ExitTimeFromZero */
/*@{*/


double q_exit_time_fz(double value);

double p_exit_time_fz(double time);

double r_exit_time_fz(gsl_rng *rng);


double r_exit_time_gets_fz(gsl_rng *rng, double t);

double r_pos_gne_fz(gsl_rng *rng, double t);

/*@}*/

#endif
