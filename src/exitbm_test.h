/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011,2012
**********************************************************************/
/** \file exitbm_test.h

\brief Code to test the library \a exitbm.

The names are of type \a test_functon, where \a function is the name of a function (for compatibility with
the code generator \a test_function.rb in ruby).

Uses \c exitbm
\c exitbm_fz.c
\c exitbm_square.c
\c exitbm_fhor.c
and 
\c exitbm_rectangle.c

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#ifndef EXITBM_TEST_H
#define EXITBM_TEST_H


/**\defgroup TestFunctions Test of functions.*/
/*@{*/

#include "exitbm.h"
#include "exitbm_fz.h"
#include "exitbm_fhor.h"
#include "exitbm_square.h"
#include "exitbm_rectangle.h"
#include "assert.h"

void test_p_exit_time(FILE *stream, double x, 
	double max_time, double step);

void test_d_exit_time(FILE *stream, double x, 
	double max_time, double step);

void test_p_exit_time_right(FILE *stream, double x, 
	double max_time, double step);

void test_p_pos_gne(FILE *stream, double t, double x, 
	 double step);

void test_d_pos_gne(FILE *stream, double t, double x, 
	 double step);

void test_p_exit_time_hypercube(FILE *stream, unsigned int dim,
	double max_time, double step);

void test_r_exit_time(FILE *stream,unsigned int nb,double x);

void test_r_exit_time_square(FILE *stream,unsigned int nb);

void test_r_exit_time_square_gets(FILE *stream,unsigned int nb,double t);

void test_r_exit_time_hypercube_gets(FILE *stream,unsigned int nb,double t, unsigned int dim);

void test_r_exit_time_position_before_horizon(FILE * stream, unsigned int N, double horizon,double init_pos);

void test_r_exit_time_position_rectangle(FILE * stream,unsigned int nb,double half_width,double half_height,double init_x,double init_y);

void test_r_exit_time_position_rectangle_rws(FILE * stream,unsigned int nb,double half_width,double half_height,double init_x,double init_y);

void test_r_exit_time_position_space_time_rectangle(
	FILE * stream,unsigned int nb,
	double half_width,double half_height,
	double final_time,double init_x,double init_y);

void test_r_exit_time_position_space_time_rectangle_rws(
	FILE * stream,unsigned int nb,
	double half_width,double half_height,
	double final_time,double init_x,double init_y);

void test_r_exit_time_hypercube(FILE * stream, unsigned int nb, unsigned int dim);
/*@}*/
#endif 
