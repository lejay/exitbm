/***********************************************************************
This file is a part of the exitbm library 
(simulation of the first exit time and position for the Brownian motion
in simple domains)

http://exitbm.gforge.inria.fr

This work is being provided under the free software CeCILL licence
Antoine Lejay (TOSCA Project-team, INRIA), 2009,2011
**********************************************************************/
/** \file exitbm_test.c

\brief Code to test the library \a exitbm.

The names are of type \a test_functon, where \a function is the name of a function (for compatibility with
the code generator \a test_function.rb in ruby).

Uses \c exitbm
\c exitbm_fz.c
\c exitbm_square.c
\c exitbm_fhor.c
and 
\c exitbm_rectangle.c

@author Antoine Lejay
@version 2.3
@date April 20, 2012
*/
#include "exitbm_test.h"

/**\addtogroup TestFunctions */
/*@{*/

/**\def INIT_GENERATOR
 * \brief Definition of random number generator called \a rng.
 */
#define INIT_GENERATOR gsl_rng * rng; const gsl_rng_type * type_rng; gsl_rng_env_setup(); type_rng=gsl_rng_default; rng=gsl_rng_alloc(type_rng);

/** \brief Print in a stream the distribution function of the first exit time \a p_exit_time.
 *
 * \param[in] stream Stream
 * \param[in] x Starting point
 * \param[in] max_time Maximum of times
 * \param[in] step Step of advance the time
 */

void test_p_exit_time(FILE *stream, double x, 
	double max_time, double step)
{
    double t;
    for(t=0.0001;t<=max_time;t+=step)
	fprintf(stream,"%f\t%f\n",t,p_exit_time(t,x));
}

/** \brief Print in a stream the density of the first exit time \a d_exit_time.
 *
 * \param[in] stream Stream
 * \param[in] x Starting point
 * \param[in] max_time Maximum of times
 * \param[in] step Step of advance the time
 */

void test_d_exit_time(FILE *stream, double x, 
	double max_time, double step)
{
    double t;
    for(t=0.0001;t<=max_time;t+=step)
	fprintf(stream,"%f\t%f\n",t,d_exit_time(t,x));
}

/** \brief Print in a stream the distribution function of the first exit time \a p_exit_time_right.
 *
 * \param[in] stream Stream
 * \param[in] x Starting point
 * \param[in] max_time Maximum of times
 * \param[in] step Step of advance the time
 */

void test_p_exit_time_right(FILE *stream, double x, 
	double max_time, double step)
{
    double t;
    for(t=0.0001;t<=max_time;t+=step)
	fprintf(stream,"%f\t%f\n",t,p_exit_time_right(t,x));
}

/** \brief Print in a stream the distribution function of the first exit time \a d_exit_time_right.
 *
 * \param[in] stream Stream
 * \param[in] t Time
 * \param[in] x Starting point
 * \param[in] step Step of advance the time
 */

void test_p_pos_gne(FILE *stream, double t, double x, 
	 double step)
{
    double y;
    for(y=-0.99;y<=0.99;y+=step)
	fprintf(stream,"%f\t%f\n",y,p_pos_gne(y,t,x));
}

/** \brief Print in a stream the density of the first exit time \a d_exit_time_right.
 *
 * \param[in] stream Stream
 * \param[in] t Time
 * \param[in] x Starting point
 * \param[in] step Step of advance the time
 */

void test_d_pos_gne(FILE *stream, double t, double x, 
	 double step)
{
    double y;
    for(y=-0.99;y<=0.99;y+=step)
	fprintf(stream,"%f\t%f\n",y,d_pos_gne(y,t,x));
}

/** \brief Print in a stream the distribution function of the first exit time from an hypercube
 *
 * \param[in] stream Stream
 * \param[in] dim Dimension
 * \param[in] max_time Maximum of times
 * \param[in] step Step of advance the time
 */

void test_p_exit_time_hypercube(FILE *stream, unsigned int dim,
	double max_time, double step)
{
    double t;
    for(t=0.0001;t<=max_time;t+=step)
	fprintf(stream,"%f\t%f\n",t,p_exit_time_hypercube(t,dim));
}

/** \brief Print in a stream some realizations of the first exit time.
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] x Starting point
 */
void test_r_exit_time(FILE *stream,unsigned int nb,double x)
{
    INIT_GENERATOR
    unsigned int i;

    for(i=0;i<nb;i++)
	    fprintf(stream,"%f\n",r_exit_time(rng,x));
}

/** \brief Print in a stream some realizations of the first exit time from a square
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 */
void test_r_exit_time_square(FILE *stream,unsigned int nb)
{
    INIT_GENERATOR
    unsigned int i;

    for(i=0;i<nb;i++)
	    fprintf(stream,"%f\n",r_exit_time_square(rng));
}

/** \brief Print in a stream some realizations of the first exit time from a square that arises before a given value
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] t Horizon 
 */
void test_r_exit_time_square_gets(FILE *stream,unsigned int nb,double t)
{
    INIT_GENERATOR
    unsigned int i;

    for(i=0;i<nb;i++)
	    fprintf(stream,"%f\n",r_exit_time_square_gets(rng,t));
}

/** \brief Print in a stream some realizations of the first exit time from a hypercube that arises before a given value
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] t Horizon 
 * \param[in] dim Dimension of the hypercube 
 */
void test_r_exit_time_hypercube_gets(FILE *stream,unsigned int nb,double t, unsigned int dim)
{
    INIT_GENERATOR
    unsigned int i;

    for(i=0;i<nb;i++)
	    fprintf(stream,"%f\n",r_exit_time_hypercube_gets(rng,t,dim));
}

/** \brief Print in a stream some realizations of the first exit time from a rectangle using \a r_exit_time_position_rectangle_rws.
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] half_width Half of the width of the rectangle
 * \param[in] half_height Half of the height of the rectangle
 * \param[in] init_x x-coordinate of the starting point
 * \param[in] init_y y-coordinate of the starting point
 */

void test_r_exit_time_position_rectangle_rws(FILE * stream,unsigned int nb,double half_width,double half_height,double init_x,double init_y)
{
    INIT_GENERATOR

    unsigned int i;

    double time,x,y;
    int side;

    assert(fabs(init_x)<half_width);
    assert(fabs(init_y)<half_height);
	
    for(i=0;i<nb;i++)
    {
	r_exit_time_position_rectangle_rws(rng,half_width,half_height,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
}

/** \brief Print in a stream some realizations of the first exit time from a rectangle using \a r_exit_time_position_rectangle.
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] half_width Half of the width of the rectangle
 * \param[in] half_height Half of the height of the rectangle
 * \param[in] init_x x-coordinate of the starting point
 * \param[in] init_y y-coordinate of the starting point
 */

void test_r_exit_time_position_rectangle(FILE * stream,unsigned int nb,double half_width,double half_height,double init_x,double init_y)
{
    INIT_GENERATOR

    assert(fabs(init_x)<half_width);
    assert(fabs(init_y)<half_height);

    unsigned int i;
    double time,x,y;
    int side;
    double sq_half_width=half_width*half_width;
    double sq_half_height=half_height*half_height;
	
    for(i=0;i<nb;i++)
    {
	r_exit_time_position_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
}


/** \brief Print in a stream some realizations of the first exit time from a space-time rectangle using \a r_exit_time_position_space_time_rectangle.
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] half_width Half of the width of the rectangle
 * \param[in] half_height Half of the height of the rectangle
 * \param[in] final_time Final time
 * \param[in] init_x x-coordinate of the starting point
 * \param[in] init_y y-coordinate of the starting point
 */

void test_r_exit_time_position_space_time_rectangle(FILE * stream,unsigned int nb,double half_width,double half_height,
	double final_time,double init_x,double init_y)
{
    INIT_GENERATOR

    assert(fabs(init_x)<half_width);
    assert(fabs(init_y)<half_height);

    unsigned int i;
    double time,x,y;
    int side;
    double sq_half_width=half_width*half_width;
    double sq_half_height=half_height*half_height;
	
    for(i=0;i<nb;i++)
    {
	r_exit_time_position_space_time_rectangle(rng,half_width,half_height,sq_half_width,sq_half_height,final_time,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
}

/** \brief Print in a stream some realizations of the first exit time from a space-time rectangle using \a r_exit_time_position_space_time_rectangle_rws.
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] half_width Half of the width of the rectangle
 * \param[in] half_height Half of the height of the rectangle
 * \param[in] final_time Final time
 * \param[in] init_x x-coordinate of the starting point
 * \param[in] init_y y-coordinate of the starting point
 */

void test_r_exit_time_position_space_time_rectangle_rws(FILE * stream,unsigned int nb,double half_width,double half_height,
	double final_time,double init_x,double init_y)
{
    INIT_GENERATOR

    assert(fabs(init_x)<half_width);
    assert(fabs(init_y)<half_height);

    unsigned int i;
    double time,x,y;
    int side;
	
    for(i=0;i<nb;i++)
    {
	r_exit_time_position_space_time_rectangle_rws(rng,half_width,half_height,final_time,init_x,init_y,&time,&x,&y,&side);
	fprintf(stream,"%i\t%f\t%f\t%f\n",side,time,x,y);
    }
}

/** \brief Print in a stream some realization of the first exit time from an hypercube. 
 *
 * \param[in] stream Stream
 * \param[in] nb Number of realizations
 * \param[in] dim Dimension
 */

void test_r_exit_time_hypercube(FILE * stream, unsigned int nb, unsigned int dim)
{
    INIT_GENERATOR
    unsigned int i;
    for(i=0;i<nb;i++)
	fprintf(stream,"%f\n",r_exit_time_hypercube(rng,dim));
}


/** \defgroup test_bm_fhor Tests for the functions related to the Brownian motion.
 * \brief Tests for the functions related to the Brownian motion.
 */

/** \defgroup test_sbm_fhor Tests for functions related to the Skew Brownian motion.
 * \brief Tests for the functions related to the Skew Brownian motion.
 */
//@}


/** @addtogroup test_bm_fhor */
//@{
//
/** \brief Generate random variates corresponding to the position of the Skew Brownian motion when it exits from \f$[-1,1]\f$ starting from \p init_pos or its position at time \p horizon.
 *
 * @param stream stream where the output are written (one per line).
 * @param N number of samples
 * @param horizon The horizon
 * @param init_pos Initial position
 */

void test_r_exit_time_position_before_horizon(FILE * stream, unsigned int N, double horizon,double init_pos)
{
    INIT_GENERATOR
    double pos;
    double time;
    int exit;
    unsigned int i;
    for(i=0;i<N;i++)
	{
	r_exit_time_position_before_horizon(rng,horizon,init_pos,&time,&pos,&exit);
	fprintf(stream,"%i\t%f\t%f\n",exit,time,pos);
	}
}
//@}

/** @addtogroup test_bm_fhor */
//@{
//
/** \brief Generate random variates corresponding to the position of the Skew Brownian motion when it exits from \f$[-1,1]\f$ starting from zero or its position at time \p horizon.
 *
 * @param N number of samples
 * @param rng Random number generator
 * @param stream Stream where the output are written (one per line).
 * @param horizon The horizon
 */

void generate_r_exit_time_position_before_horizon_fz(int N,gsl_rng *rng,FILE *stream,double horizon)
{
    double pos;
    double time;
    int exit;
    int i;
    printf("Proba. de sortie avant l'horizon = %f\n",p_exit_time_fz(horizon));
    for(i=0;i<N;i++)
	{
	r_exit_time_position_before_horizon_fz(rng,horizon,&time,&pos,&exit);
	fprintf(stream,"%i\t%f\t%f\n",exit,time,pos);
	}
}
//@}
//
/** @addtogroup test_bm_fhor */
//@{
//
/** \brief Generate random variates corresponding to the position of the Skew Brownian motion when it exits from an interval or its position at time \p horizon.
 *
 * @param stream Stream where the output are written (one per line).
 * @param N number of samples
 * @param inter Pointer to the interval
 * @param horizon The horizon
 */
void test_r_exit_time_position_before_horizon_from_interval(FILE * stream, unsigned int N, interval * inter, double horizon)
{
    INIT_GENERATOR
    double pos, time;
    int exit;
    unsigned int i;
    for(i=0;i<N;i++)
    {
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon,&time,&pos,&exit);
	fprintf(stream,"%i\t%f\t%f\n",exit,time,pos);
    }
}

//@}

/** @addtogroup test_sbm_fhor */
//@{
//
/** \brief Generate random variates corresponding to the position of the Skew Brownian motion when it exits from \f$[-1,1]\f$ or its position at time \p horizon.
 *
 * @param stream Stream where the output are written (one per line).
 * @param N number of samples
 * @param horizon The horizon
 * @param skewness The skewness parameter (in \f$(0,1)\f$).
 */

void test_r_exit_time_position_before_horizon_sbm(FILE *stream, unsigned int N, double horizon,double skewness)
{
    INIT_GENERATOR
    double pos;
    double time;
    int exit;
    unsigned int i;
    for(i=0;i<N;i++)
	{
	r_exit_time_position_before_horizon_sbm(rng,horizon,skewness,&time,&pos,&exit);
	fprintf(stream,"%i\t%f\t%f\n",exit,time,pos);
	}
}
//@}

/** @addtogroup test_sbm_fhor */
//@{
//
/** \brief Generate random variates corresponding to the position of the Skew Brownian motion when it exits from the centered interval \p inter or its position at time \p horizon.
 *
 * @param stream Stream where the output are written (one per line).
 * @param N number of samples
 * @param inter Pointer to the centered interval
 * @param horizon The horizon
 * @param skewness The skewness parameter (in \f$(0,1)\f$).
 */
void test_r_exit_time_position_before_horizon_from_interval_sbm(FILE * stream, unsigned int N, cent_interval * inter, double horizon, double skewness)
{
    INIT_GENERATOR
    double pos, time;
    int exit;
    unsigned int i;
    for(i=0;i<N;i++)
    {
	r_exit_time_position_before_horizon_from_interval_sbm(rng,inter,horizon,skewness,&time,&pos,&exit);
	fprintf(stream,"%i\t%f\t%f\n",exit,time,pos);
    }
}
//@}


/** @addtogroup test_bm_fhor */
//@{
/** \brief Construct a Gaussian distribution by jumping from an integer to the other integer.
 Uses \p r_exit_time_position_before_horizon_fz
 The density of the particles shall be equal to the one of the Gaussian distribution with
 zero mean and variance \p horizon.
 @retval Position at time \p horizon */
double check_gaussianity_fz(gsl_rng *rng,double horizon,double init)
{
    double time,pos,exit_time;
    int exit,point;
    time=0.0;

    if (init==floor(init)) 
	{ point =(int) floor(init); }
   else
	{
	interval *inter=interval_new(floor(init),ceil(init),init);
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon,&exit_time,&pos,&exit);
	free(inter);
	time+=exit_time;
	if (!exit)
	{ return(pos);}
	point=(int) (exit==1 ? ceil(init) : floor(init));
    }
    while(1)
    {
	r_exit_time_position_before_horizon_fz(rng,horizon-time,&exit_time,&pos,&exit);
	time+=exit_time;	
	if (exit==0) 
	    { return(point+pos); }
	point+=exit; // shift the point
    }
} 


//! \brief Construct the density of the Skew Brownian motion by jumping from an integer to the other integer.
/** Uses \p r_exit_time_position_before_horizon_fz
 @param rng Random number generator
 @param skew The skewnewss parameter in \f$[0,1]\f$
 @param horizon The time horizon
 @param init The initіal position
 @retval Position at time \p horizon
 */
double check_sbm(gsl_rng *rng,double skew,double horizon,double init)
{
    double time,pos,exit_time;
    int exit,point;
    point=0;
    time=0.0;

    if (init==0) 
	{ point =0; }
   else
	{
	interval *inter=interval_new(floor(init),ceil(init),init);
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon,&exit_time,&pos,&exit);
	free(inter);
	time+=exit_time;
	if (!exit)
	{ return(pos);}
	point=(int) (exit==1 ? ceil(init) : floor(init));
    }

    while(1)
    {
	if(point==0)
	{ r_exit_time_position_before_horizon_sbm(rng,horizon,skew,&exit_time,&pos,&exit);}
	else
	{ r_exit_time_position_before_horizon_fz(rng,horizon-time,&exit_time,&pos,&exit);}
	time+=exit_time;	
	if (exit==0) 
	    { return(point+pos); }
	point+=exit; // shift the point
    }
} 

//! \brief Construct a Gaussian distribution by jumping from an integer to the other integer.
/** Uses \p r_exit_time_position_before_horizon
 The density of the particles shall be equal to the one of the Gaussian distribution with
 mean \p init and variance \p horizon.

 @param rng Random number generator
 @param horizon The time horizon
 @param init The initіal position

 @retval Position at time \p horizon
 */
double check_gaussianity(gsl_rng *rng,double horizon, double init)
{
    double time,pos,exit_time;
    int exit,point;
    point=0;
    time=0.0;

    if (init==0) 
	{ point =0; }
   else
	{
	interval *inter=interval_new(floor(init),ceil(init),init);
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon,&exit_time,&pos,&exit);
	free(inter);
	time+=exit_time;
	if (!exit)
	{ return(pos);}
	point=(int) (exit==1 ? ceil(init) : floor(init));
    }
    while(1)
    {
	r_exit_time_position_before_horizon(rng,horizon-time,0.0,&exit_time,&pos,&exit);
	time+=exit_time;	
	if (exit==0) 
	    { return(point+pos); }
	point+=exit; // shift the point
    }
} 

//! \brief Construct a Gaussian distribution by jumping from an integer to the other integer.
/** Uses \p r_exit_time_position_before_horizon_from_interval

 The density of the particles shall be equal to the one of the Gaussian distribution with
 mean \p init and variance \p horizon.

 The Gaussianity can be studied by comparing the mean, the variance, the density, or using a quantile-quantile plot. 

 @param rng Random number generator
 @param horizon The time horizon
 @param init The initіal position

 @retval Position at time \p horizon
 */
double check_gaussianity_from_interval(gsl_rng *rng,double horizon, double init)
{
    interval *inter;
    inter=interval_new(-1.0,1.0,0.0);
    double time,pos,exit_time;
    int exit,point;
    point=0;
    time=0.0;

    if (init==0) 
	{ point =0; }
   else
	{
	interval *inter=interval_new(floor(init),ceil(init),init);
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon,&exit_time,&pos,&exit);
	free(inter);
	time+=exit_time;
	if (!exit)
	{ return(pos);}
	point=(int) (exit==1 ? ceil(init) : floor(init));
    }
    while(1)
    {
	r_exit_time_position_before_horizon_from_interval(rng,inter,horizon-time,&exit_time,&pos,&exit);
	time+=exit_time;	
	if (exit==0) 
	    { return(point+pos); }
	point+=exit; // shift the point
    }
} 
//@}

/*@}*/


